/*FullCalendar Init*/
$(document).ready(function() {
	'use strict';
    //pindah room -> calendar.blade.php
    // var drag =  function() {
    //     $('.calendar-event').each(function() {

    //     	// store data so the calendar knows to render an event upon drop
	//         // $(this).data('event', {
	//         //     title: $.trim($(this).find('p').text()), // use the element's text as the event title
	//         //     stick: false // maintain when user navigates (see docs on the renderEvent method)
	//         // });
	//         // make the event draggable using jQuery UI
	//         $(this).draggable({
	//             zIndex: 1111999,
	//             revert: true,      // will cause the event to go back to its
	//             revertDuration: 0  //  original position after the drag
	//         });
    // 	});
	// };
	var globalTitle = undefined;
    var globalIdShift = undefined;

    $(document).on('click', '.calendar-event', function(e) {

		// console.log($(this).data('id'));
		// console.log($(this).find('p').text());
        if ($(this).hasClass('alert-secondary')) {
			// console.log('asd');
            $('.calendar-event').removeClass('alert-primary');
            $('.calendar-event').addClass('alert-secondary');
            $(this).removeClass('alert-secondary');
            $(this).addClass('alert-primary');
			globalIdShift = $(this).data('id');

            globalTitle = $.trim((($(this).find('p').text()).split(" "))[1]);
        } else {
            globalIdShift = undefined;
            globalTitle = undefined;
            $(this).removeClass('alert-primary');
            $(this).addClass('alert-secondary');
        }
	});

	$(document).on('mousedown', '.calendar-event', function() {
		globalIdShift = $(this).data('id');
		globalTitle = $.trim((($(this).find('p').text()).split(" "))[1]);
	})

    // var removeEvent =  function() {
    //     $(document).on('click','.remove-calendar-event',function(e) {
    //         $(this).closest('.calendar-event').fadeOut();
    //         return false;
    //     });
    // };

    // $(document).on('click', '#add_event',function(e) {
	// 	$('<div class="calendar-event alert alert-secondary alert-dismissible fade show"><p>' + $('#inputEvent').val() + '</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').insertAfter(".calendar-event:last-child");
	// 	$(this).parents('.modal').find('.close').trigger('click');
	// 	return false;
	// });

    // drag();
    // removeEvent();

    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();

	var cal = $('#calendar').fullCalendar({
		year: 2012,
		month: 4,
		date: 25,
        themeSystem: 'bootstrap4',
        customButtons: {
            calendarSidebar: {
                text: 'icon',
            }
        },
        header: {
        left: 'calendarSidebar ,today',
        center: 'prev,title,next',
        right: 'month,agendaWeek,agendaDay,listMonth'
        },
        droppable: true,
        editable: false,
        height: 'parent',
        eventLimit: true, // allow "more" link when too many events
        windowResizeDelay:500,
        events: [

            ],
        dayClick: function(start, end, jsEvent, view) {
            var events = $('#calendar').fullCalendar('clientEvents');
            let eventToday = [];
            console.log(globalTitle);
            for(var i = 0 ; i < events.length ; i++){
                if (start.format() == events[i].start._i) {
                    eventToday.push(events[i]);
                }
            }
            if (eventToday.length > 0) {
                $('#calendar').fullCalendar( 'removeEvents', eventToday[0]._id );
            }
            if (globalTitle != '' && globalTitle != null) {
                var newEvent = new Object();
                newEvent.title = globalTitle;
                newEvent.id = globalIdShift+'_'+moment(start).format();
                newEvent.start = moment(start).format();
                $('#calendar').fullCalendar('renderEvent', newEvent);
            }
        },
        drop: function(start, end, jsEvent, view) {
            var events = $('#calendar').fullCalendar('clientEvents');

            let eventToday = [];
            for(var i = 0 ; i < events.length ; i++){
                if (start.format() == events[i].start._i) {
                    eventToday.push(events[i]);
                }
            }
            if (eventToday.length > 0) {
                $('#calendar').fullCalendar( 'removeEvents', eventToday[0]._id );
            }
                var newEvent = new Object();
                newEvent.title = globalTitle;
                newEvent.id = globalIdShift+'_'+moment(start).format();
                // newEvent.id = globalIdShift;
                newEvent.start = moment(start).format();
                // console.log(moment(start).format());
                $('#calendar').fullCalendar('renderEvent', newEvent);
            // if (events.length > 1) {
            // 	$('#calendar').fullCalendar( 'removeEvents', events._id );

            // }
            if($("#remove_event").is(':checked'))
                $(this).remove();
        },
        eventClick: function(event) {
            console.log($('#calendar').fullCalendar('clientEvents'));
            console.log(event._id);
            $('#calendar').fullCalendar( 'removeEvents', event.id );
        },
	});
	setTimeout(function(){
		$('.fc-left .fc-calendarSidebar-button').attr("id","calendarapp_sidebar_move").html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>');
		$('.fc-left .fc-today-button').removeClass('btn-primary').addClass('btn-outline-secondary btn-sm');
		$('.fc-center .btn').removeClass('btn-primary').addClass('btn-outline-light btn-sm');
		$('.fc-right .btn-group').addClass('btn-group-sm');
		$('.fc-right .btn').removeClass('btn-primary').addClass('btn-outline-secondary');
	},100);

	$('#wokeh').click(function(){
		$('#calendar').fullCalendar( 'removeEvents', '_fc1' )
	});
});
