<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    protected $primaryKey = 'id_jabatan';
    protected $guarded = [];
    public $timestamps = false;

    public function karyawans()
    {
        return $this->hasMany('App\Karyawan', 'id_jabatan', 'id_jabatan');
    }
}
