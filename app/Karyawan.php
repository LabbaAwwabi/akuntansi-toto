<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    //
    protected $table = 'karyawan';
    protected $primaryKey = 'id_karyawan';
    protected $guarded = [];
    protected $casts = [
        'prn' => 'string',
        'tgl_masuk' => 'datetime:Y-m-d',
    ];

    public function getTunjanganJabatanAttribute() {
        $prn = $this->attributes['prn'];

        if (substr($prn, 0, 2) == '05') {
            return 0;
        }

        return $this->jabatan->tunjangan_jabatan;
    }

    public function getNamaAttribute() {
        if (isset($this->attributes['nama'])) {
            return ucwords($this->attributes['nama']);
        } else {
            return null;
        }
    }

    public function setNamaAttribute($value) {
        $this->attributes['nama'] = ucwords($value);
    }

    public function getTglMasukAttribute() {
        if (isset($this->attributes['tgl_masuk'])) {
            return Carbon::parse($this->attributes['tgl_masuk'])->format('d/m/Y');
        } else {
            return '';
        }
    }

    public function getCarbonTglMasukAttribute() {
        if (isset($this->attributes['tgl_masuk'])) {
            return Carbon::parse($this->attributes['tgl_masuk']);
        } else {
            return null;
        }
    }

    public function setTglMasukAttribute($value) {
        $this->attributes['tgl_masuk'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d') . " 00:00:00";
    }

    public function jabatan(){
        return $this->belongsTo('App\Jabatan', 'id_jabatan', 'id_jabatan');
    }

    public function getRealValue($percent)
    {
        return $this->attributes['gaji_pokok'] * ($percent / 100);
    }
}
