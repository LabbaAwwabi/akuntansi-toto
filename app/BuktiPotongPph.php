<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuktiPotongPph extends Model
{
    protected $table = 'bukti_potong_pph';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function karyawan() {
        return $this->belongsTo('App\Karyawan', 'id_karyawan', 'id_karyawan');
    }
}
