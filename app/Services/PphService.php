<?php

namespace App\Services;

use App\Karyawan;
use App\Payroll;
use App\Pph;

class PphService
{
    private $karyawan;
    private $payroll;
    private $pphModel;
    private $pphTerbayar;
    private $counterBulan;

    public function __construct(Payroll $payroll)
    {
        $this->payroll = $payroll;
        $this->karyawan = Karyawan::find($payroll->id_karyawan);
        $this->pphModel = new Pph();
        $this->pphModel->id_payroll = $payroll->id_payroll;
        $this->pphModel->id_karyawan = $payroll->id_karyawan;
        $this->pphTerbayar = 0;
        $this->counterBulan = 0;
    }

    public function getValue(): int
    {
        $pph = 0;
        $bulanBekerja = $this->getBulanBekerja();
        $gajiBersihSetahun = $this->getPenghasilanBersihSetahun($bulanBekerja);

        $pkpSetahun = $gajiBersihSetahun - (int)$this->karyawan->ptkp;
        $this->pphModel->ptkp = (int)$this->karyawan->ptkp;

        //pembulatan kebawah
        $excess = $pkpSetahun % 1000;
        $pkpSetahun -= $excess;

        $this->pphModel->pkp = $pkpSetahun;

        if ($pkpSetahun < 0) {
            $pphBulanan = 0;
            $this->payroll->pph = $pphBulanan;
            $this->payroll->save();
            $this->pphModel->pph = $pphBulanan;
            $this->pphModel->save();

            return $pphBulanan;
        }

        /**
         * TODO: buat UI (ditaruh sebelah mana?)
         * Ans: dibuatkan menu PPH21
         */
        // 0-50
        if ($pkpSetahun > 50000000) {
            $pph += 50000000 * ($this->karyawan->npwp ? 5 : 6) / 100;
            $pkpSetahun -= 50000000;
        } else {
            $pph += $pkpSetahun * ($this->karyawan->npwp ? 5 : 6) / 100;

            return $this->setPphBulanan($pph);
        }

        if ($pkpSetahun > 250000000) {
            $pph += 250000000 * ($this->karyawan->npwp ? 15 : 18) / 100;
            $pkpSetahun -= 250000000;
        } else {
            $pph += $pkpSetahun * ($this->karyawan->npwp ? 15 : 18) / 100;
            // dd($pph, $pkpSetahun); die();
            return $this->setPphBulanan($pph);
        }

        if ($pkpSetahun > 500000000) {
            $pph += 500000000 * ($this->karyawan->npwp ? 25 : 30) / 100;
            $pkpSetahun -= 500000000;
        } else {
            $pph += $pkpSetahun * ($this->karyawan->npwp ? 25 : 30) / 100;

            return $this->setPphBulanan($pph);
        }

        $pph += $pkpSetahun * ($this->karyawan->npwp ? 30 : 36) / 100;

        return $this->setPphBulanan($pph);
    }

    private function getPenghasilanBersihSetahun($bulanBekerja): int
    {
        $payrolls = Payroll::where('id_karyawan', $this->karyawan->id_karyawan)
                    ->where('tahun', $this->payroll->tahun)
                    ->where('bulan', '<=', $this->payroll->bulan)
                    ->orderBy('bulan', 'asc')
                    ->get();
        $counter = $bulanBekerja;
        $totalPenghasilan = 0;

        foreach ($payrolls as $payroll) {
            if ($payroll->bulan < $this->payroll->bulan) {
                $pphModel = Pph::where('id_payroll', $payroll->id_payroll)->first();
                if ($pphModel !== null) {
                    $totalPenghasilan += $pphModel->gaji_bersih;
                    $this->pphTerbayar += $pphModel->pph;
                    $counter--;
                }
            } else {
                $gajiFinal = $this->getGajiFinal($payroll);
                $this->pphModel->gaji_bersih = $gajiFinal;
                $totalPenghasilan += $gajiFinal * $counter;
                $this->counterBulan = $counter;
                break;
            }

        }

        return $totalPenghasilan;
    }

    /**
     * @param Payroll $payroll
     * Rumus perhitungan gaji final, sebelum dihitung pph
     * @return int
     */
    private function getGajiFinal(Payroll $payroll): int
    {
        $gajiFinal = 0;
        $tunjanganJabatan = (int)$this->karyawan->tunjangan_jabatan;

        $gajiFinal += $this->karyawan->gaji_pokok;
        $this->pphModel->gaji_pokok = (int)$this->karyawan->gaji_pokok;
        $gajiFinal += $tunjanganJabatan;
        $this->pphModel->tunjangan_jabatan = $tunjanganJabatan;
        $gajiFinal += $payroll->lembur + $payroll->uang_makan;
        $this->pphModel->lembur = $payroll->lembur + $payroll->uang_makan;
        $gajiFinal += $payroll->shift;
        $this->pphModel->shift = $payroll->shift;
        $gajiFinal += ($payroll->tambahan_absen - $payroll->absen);
        $this->pphModel->absen = $payroll->absen;
        $gajiFinal += $payroll->bpjs_kesehatan_p_out;
        $this->pphModel->tunjangan_kesehatan = $payroll->bpjs_kesehatan_p_out;
        $gajiFinal += $payroll->bpjs_jkk_p_out;
        $this->pphModel->tunjangan_jkk = $payroll->bpjs_jkk_p_out;
        $gajiFinal += $payroll->bpjs_jkm_p_out;
        $this->pphModel->tunjangan_jkm = $payroll->bpjs_jkm_p_out;

        $potonganTunjanganJabatan = (int)($gajiFinal * (5 / 100));
        $potonganTunjanganJabatan = $potonganTunjanganJabatan <= 500000 ? $potonganTunjanganJabatan : 500000;
        $gajiFinal -= $potonganTunjanganJabatan;
        $this->pphModel->potongan_tunjangan_jabatan = $potonganTunjanganJabatan;
        $gajiFinal -= $payroll->bpjs_jht_out;
        $this->pphModel->potongan_jht = $payroll->bpjs_jht_out;
        $gajiFinal -= $payroll->bpjs_jp_out;
        $this->pphModel->potongan_jp = $payroll->bpjs_jp_out;

        return $gajiFinal;
    }

    private function getBulanBekerja(): int
    {
        $tahunPph = $this->payroll->tahun;

        if ($tahunPph === substr($this->karyawan->tgl_masuk, 0, 4)) {
            return 13 - (int)substr($this->karyawan->tgl_masuk, 5, 2);
        }

        return 12;
    }

    private function setPphBulanan($pph)
    {
        $pphBulanan = round(($pph - $this->pphTerbayar)/$this->counterBulan);
        $this->payroll->pph = (int)$pphBulanan;
        $this->payroll->tunjangan_pph = (int)$pphBulanan;
        $this->payroll->save();
        $this->pphModel->pph = (int)$pphBulanan;
        $this->pphModel->save();

        return $pphBulanan;
    }
}
