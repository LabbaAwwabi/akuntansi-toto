<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $table = 'payroll';
    protected $primaryKey = 'id_payroll';
    protected $guarded = [];

    public function karyawan() {
        return $this->belongsTo('App\Karyawan', 'id_karyawan', 'id_karyawan');
    }
}
