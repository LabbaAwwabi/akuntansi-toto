<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pph extends Model
{
    protected $table = 'pph';
    protected $primaryKey = 'id_pph';
    protected $guarded = [];

    public function karyawan() {
        return $this->belongsTo('App\Karyawan', 'id_karyawan', 'id_karyawan');
    }

    public function payroll() {
        return $this->belongsTo('App\Payroll', 'id_payroll', 'id_payroll');
    }
}
