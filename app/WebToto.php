<?php

namespace App;

class WebToto
{

    public function __construct()
    {
        $this->base_url = (Variable::where('nama', 'base_url_api')->first())->value;
        $this->api_token = null;
        $this->api_host = 'localhost';
        $this->api_port = 3000;
        $this->api_username = (Variable::where('nama', 'username')->first())->value;
        $this->api_password = (Variable::where('nama', 'password')->first())->value;
        $this->client_conn = new \GuzzleHttp\Client();
    }
    public function getAllJabatan()
    {
        if($this->api_token == null) {
            $this->apiLogin();
        }
        if($this->api_token != null) {
            $response = $this->client_conn->request('GET', 'http://'.$this->base_url.'/api/jabatan', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                    'Authorization'     => 'Bearer '.$this->api_token,
                ]
            ]);

            return json_decode($response->getBody()->getContents());
        }

        return [];
    }

    public function getAllKaryawan()
    {
        if($this->api_token == null) {
            $this->apiLogin();
        }

        if($this->api_token != null) {
            $response = $this->client_conn->request('GET', 'http://'.$this->base_url.'/api/karyawan', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                    'Authorization'     => 'Bearer '.$this->api_token,
                ]
            ]);

            return json_decode($response->getBody()->getContents());
        }

        return [];
    }

    public function getPayroll($prn, $bulan, $tahun)
    {
        if($this->api_token == null) {
            $this->apiLogin();
        }

        if($this->api_token != null) {
            $request = $this->client_conn->request('POST', 'http://'.$this->base_url.'/api/payroll', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                    'Authorization'     => 'Bearer '.$this->api_token,
                ],
                'body' => json_encode([
                    "prn" => $prn,
                    "bulan" => $bulan,
                    "tahun" => $tahun,
                ])
            ]);

            return json_decode($request->getBody()->getContents());
        }

        return [];
    }

    private function apiLogin() {
        try {
            $response = $this->client_conn->request('POST', 'http://'.$this->base_url.'/api/login', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                ],
                'body' => json_encode([
                    "email" => $this->api_username ,
                    "password" => $this->api_password ,
                ])
            ]);

            $response = json_decode($response->getBody()->getContents());

            if(array_key_exists("api_token", $response->data)) {
                $this->api_token = $response->data->api_token;
                return $this->api_token;
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return dd($e->getMessage());
        }

        return null;
    }
}
