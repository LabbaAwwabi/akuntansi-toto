<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public static function getVariable($varName)
    {
        $var = Variable::where('nama', $varName)->first();

        if ($var) {
            return $var;
        } else {
            return Variable::create([
                'nama' => $varName,
                'label' => ucwords(str_replace('_', ' ', $varName)),
                'value' => '']);
        }
    }

    public static function getValue($varName)
    {
        $var = Variable::where('nama', $varName)->first();

        if ($var == null) {
            $var = Variable::create([
                'nama' => $varName,
                'label' => ucwords(str_replace('_', ' ', $varName)),
                'value' => '']);
        }

        return $var->value;
    }
}
