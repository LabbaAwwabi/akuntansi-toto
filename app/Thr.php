<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thr extends Model
{
    protected $table = 'thr';
    protected $primaryKey = 'id_thr';
    protected $guarded = [];

    public function karyawan() {
        return $this->belongsTo('App\Karyawan', 'id_karyawan', 'id_karyawan');
    }
}
