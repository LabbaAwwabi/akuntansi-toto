<?php

namespace App\Http\Controllers;

use App\BuktiPotongPph;
use App\Pph;
use App\Variable;
use Carbon\Carbon;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PphController extends Controller
{
    public function index()
    {
        $seksi = DB::table('karyawan')->select(DB::raw('DISTINCT seksi'))->get()->toArray();
        return view('pph.index', compact('seksi'));
    }

    public function getData(Request $request)
    {
        $wheres = [];
        $tglThr = new Carbon();

        if ($request->seksi != null) {
            $wheres['karyawan.seksi'] = $request->seksi;
        }

        if ($request->id_jabatan != null) {
            $wheres['karyawan.id_jabatan'] = $request->id_jabatan;
        }

        if ($request->tanggal_thr != null) {
            $tglThr = Carbon::createFromFormat('d/m/Y', $request->tanggal_thr);
        }

        if (count($wheres) > 0) {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'))
                ->where($wheres);
        } else {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'));
        }

        return Datatables::of($query)
            ->addIndexColumn()
            ->addColumn('jabatan', function ($karyawan) {
                if ($karyawan->id_jabatan !== NULL) {
                    return $karyawan->nama_jabatan;
                } else {
                    return "Tidak ada jabatan";
                }
            })
            ->addColumn('tunjangan_jabatan', function ($karyawan) {
                $k = Karyawan::find($karyawan->id_karyawan);
                if ($k) {
                    return $karyawan->tunjangan_jabatan;
                }
            })
            ->addColumn('action', function ($karyawan) {
                return view('pph.buttons', compact('karyawan'));
            })
            ->rawColumns(['action'])
            ->filterColumn('jabatan', function ($query, $keyword) {
                $sql = "jabatan.nama like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('jabatan', function ($query, $order) {
                $query->orderBy('jabatan.nama', $order);
            })
            ->toJson();
    }

    public function getDetail(Request $request)
    {
        $pph = BuktiPotongPph::where('id_karyawan', $request->id_karyawan)
            ->where('tahun', $request->tahun)
            ->first();

        return json_encode($pph);
    }

    public function update(Request $request)
    {
        $pph = BuktiPotongPph::where('id_karyawan', $request->id_karyawan)
            ->where('tahun', $request->tahun)
            ->first();

        $pph->update($request->all());

        return 'Berhasil mengupdate data karyawan!';
    }

    public function pdf(Request $request)
    {
        $data = [];
        $buktiPotongPph = BuktiPotongPph::firstOrCreate(
            ['tahun' => $request->tahun, 'id_karyawan' => $request->id_karyawan]
        );

        $data['base64'] = $this->getImage($buktiPotongPph);

        $pdf = PDF::loadView('pph.pdf', ['data' => $data], [], [
            'format'               => 'A4',
            'default_font_size'    => '12',
            'default_font'         => 'sans-serif',
            'margin_left'          => 0,
            'margin_right'         => 0,
            'margin_top'           => 0,
            'margin_bottom'        => 0,
            'margin_header'        => 0,
            'margin_footer'        => 0,
            'orientation'          => 'P',
            'title'                => 'Form 1721-A1',
        ]);

        return $pdf->stream("Form 1721-A1.pdf");
    }

    public function getImage(BuktiPotongPph $buktiPotongPph)
    {
        $formImg = 'img/1721-a1.jpg';
        $img = imagecreatefromjpeg($formImg);

        $black = imagecolorallocate($img, 0, 0, 0);
        $white = imagecolorallocate($img, 255, 255, 255);

        $fontArial = realpath('fonts/Arial.ttf');
        $fontArialBold = realpath("fonts/Arial_Bold.ttf");

        $fontSize = 14;

        if (date('Y') == $buktiPotongPph->tahun) {
            $bulan = date('m');
            $tahun = date('Y');
        } else {
            $bulan = '12';
            $tahun = $buktiPotongPph->tahun;
        }

        //nomor
        imagettftext($img, $fontSize, 0, 630, 270, $black, $fontArial, $bulan);
        imagettftext($img, $fontSize, 0, 700, 270, $black, $fontArial, substr($tahun,2));
        imagettftext($img, $fontSize, 0, 760, 270, $black, $fontArial, $buktiPotongPph->nomor);

        if (substr($buktiPotongPph->karyawan->tgl_masuk,0,4) != $buktiPotongPph->tahun) {
            $startBulan = '01';
        } else {
            $startBulan = substr($buktiPotongPph->karyawan->tgl_masuk,8);
        }

        imagettftext($img, $fontSize, 0, 1050, 270, $black, $fontArial, $startBulan);
        imagettftext($img, $fontSize, 0, 1120, 270, $black, $fontArial, $bulan);

        //perusahaan
        $npwpPerusahaan = Variable::getValue('npwp_perusahaan');
        imagettftext($img, $fontSize, 0, 320, 340, $black, $fontArial, $this->getFirstNpwp($npwpPerusahaan));
        imagettftext($img, $fontSize, 0, 590, 340, $black, $fontArial, $this->getMiddleNpwp($npwpPerusahaan));
        imagettftext($img, $fontSize, 0, 690, 340, $black, $fontArial, $this->getLastNpwp($npwpPerusahaan));
        imagettftext($img, $fontSize, 0, 320, 390, $black, $fontArial, Variable::getValue('nama_perusahaan'));

        $y = 495;
        //npwp
        imagettftext($img, $fontSize, 0, 240, $y, $black, $fontArial, $this->getFirstNpwp($buktiPotongPph->karyawan->npwp));
        imagettftext($img, $fontSize, 0, 520, $y, $black, $fontArial, $this->getMiddleNpwp($buktiPotongPph->karyawan->npwp));
        imagettftext($img, $fontSize, 0, 620, $y, $black, $fontArial, $this->getLastNpwp($buktiPotongPph->karyawan->npwp));
        //nik
        imagettftext($img, $fontSize, 0, 240, $y+=55, $black, $fontArial, $buktiPotongPph->karyawan->nik);
        //nama
        imagettftext($img, $fontSize, 0, 240, $y+=40, $black, $fontArial, $buktiPotongPph->karyawan->nama);
        //alamat
        imagettftext($img, $fontSize, 0, 240, $y+=40, $black, $fontArial, substr($buktiPotongPph->karyawan->alamat,0,40));
        //jenis kelamin
        if ($buktiPotongPph->karyawan->jenis_kelamin === 'Perempuan') {
            imagettftext($img, $fontSize, 0, 500, $y+=79, $black, $fontArial, 'X');
        } else {
            imagettftext($img, $fontSize, 0, 310, $y+=79, $black, $fontArial, 'X');
        }
        //status kawin
        if (substr($buktiPotongPph->karyawan->status_kawin,0,1) === 'K') {
            imagettftext($img, $fontSize, 0, 775, $y-=79+80, $black, $fontArial, substr($buktiPotongPph->karyawan->status_kawin,2));
        } else {
            imagettftext($img, $fontSize, 0, 920, $y-=79+80, $black, $fontArial, substr($buktiPotongPph->karyawan->status_kawin,3));
        }
        //nama jabatan
        imagettftext($img, $fontSize, 0, 910, $y+=40, $black, $fontArial, strtoupper($buktiPotongPph->karyawan->jabatan->nama));
        //kode pajak
        imagettftext($img, $fontSize, 0, 286, 843, $black, $fontArial, 'X');

        //perhitungan
        $listPph = DB::table('pph')
            ->join('payroll', 'pph.id_payroll', '=', 'payroll.id_payroll')
            ->where('payroll.tahun', '=', $buktiPotongPph->tahun)
            ->where('id_karyawan', $buktiPotongPph->id_karyawan)
            ->orderBy('bulan')
            ->get();

        $fontSize = 14;

        if (count($listPph) > 0) {
            $gajiPokok = 0;
            $tunjanganLainnya = 0;
            $premiAsuransi = 0;
            $penghasilanBruto = 0;
            $potonganTunjanganJabatan = 0;
            $potonganJhtJp = 0;
            $gajiBersihPph = 0;
            $pkp = 0;
            $pphSetahun = 0;

            foreach ($listPph as $item) {
                $gajiPokok += $item->gaji_pokok;
                $tunjanganLainnya += $item->shift + $item->lembur - $item->absen;
                $premiAsuransi += $item->tunjangan_kesehatan + $item->tunjangan_jkk + $item->tunjangan_jkm;
                $potonganTunjanganJabatan += $item->potongan_tunjangan_jabatan;
                $potonganJhtJp += $item->potongan_jht + $item->potongan_jp;
                $gajiBersihPph += $item->gaji_bersih;
                $pkp = $item->pkp;
                $pphSetahun += $item->pph;
            }

            $tglThr = Carbon::parse('2021-05-12');
            $bulanBekerja = $tglThr->diffInMonths($buktiPotongPph->karyawan->tgl_masuk);
            if ($bulanBekerja > 12) {
                $bulanBekerja = 12;
            }
            $thr = ($buktiPotongPph->karyawan->gaji_pokok + $buktiPotongPph->karyawan->tunjangan_jabatan) * ($bulanBekerja / 12);

            $penghasilanBruto += $gajiPokok + 0 + $tunjanganLainnya + 0 + $premiAsuransi + 0 + $thr;

            $y=920;
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($gajiPokok), $img, $fontSize, $fontArialBold, -80), $y, $black, $fontArialBold, $this->toRupiah($gajiPokok));
            imagettftext($img, $fontSize, 0, $this->getXstart('0', $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, '0');
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($tunjanganLainnya), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($tunjanganLainnya));
            imagettftext($img, $fontSize, 0, $this->getXstart('0', $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, '0');
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($premiAsuransi), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($premiAsuransi));
            imagettftext($img, $fontSize, 0, $this->getXstart('0', $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, '0');
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($thr), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($thr));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($penghasilanBruto), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($penghasilanBruto));

            $jumlahPotongan = $potonganTunjanganJabatan + $potonganJhtJp;
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($potonganTunjanganJabatan), $img, $fontSize, $fontArialBold, -80), $y+=75, $black, $fontArialBold, $this->toRupiah($potonganTunjanganJabatan));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($potonganJhtJp), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($potonganJhtJp));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($jumlahPotongan), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($jumlahPotongan));

            $penghasilanNeto = $penghasilanBruto - $jumlahPotongan;
            $ptkp = $buktiPotongPph->karyawan->ptkp;
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($penghasilanNeto), $img, $fontSize, $fontArialBold, -80), $y+=75, $black, $fontArialBold, $this->toRupiah($penghasilanNeto));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah(0), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $buktiPotongPph->perhitungan_13);
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($penghasilanNeto + 0), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($penghasilanNeto + 0));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($ptkp), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($ptkp));
            $pkp = $pkp > 0 ? $pkp : 0;
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($pkp), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($pkp));
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($pphSetahun), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($pphSetahun));
            //18 = input
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($pphSetahun), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($buktiPotongPph->perhitungan_18));
            //19 = 17-18
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($pphSetahun), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($pphSetahun - $buktiPotongPph->perhitungan_18));
            //20 = 19
            imagettftext($img, $fontSize, 0, $this->getXstart($this->toRupiah($pphSetahun), $img, $fontSize, $fontArialBold, -80), $y+=35, $black, $fontArialBold, $this->toRupiah($pphSetahun - $buktiPotongPph->perhitungan_18));

            //direktur
            $npwpPerusahaan = Variable::getValue('npwp_direktur');
            imagettftext($img, $fontSize, 0, 240, $y+=95, $black, $fontArial, $this->getFirstNpwp($npwpPerusahaan));
            imagettftext($img, $fontSize, 0, 500, $y, $black, $fontArial, $this->getMiddleNpwp($npwpPerusahaan));
            imagettftext($img, $fontSize, 0, 570, $y, $black, $fontArial, $this->getLastNpwp($npwpPerusahaan));
            imagettftext($img, $fontSize, 0, 240, $y+=40, $black, $fontArial, Variable::getValue('nama_direktur'));

            //tgl
            imagettftext($img, $fontSize, 0, 680, $y, $black, $fontArial, date('d'));
            imagettftext($img, $fontSize, 0, 760, $y, $black, $fontArial, date('m'));
            imagettftext($img, $fontSize, 0, 860, $y, $black, $fontArial, date('Y'));

            /*********** foto *********/
            $fotoimg = imagecreatefrompng ('img/ttd_bukti_potong_pph.png');
            $coordX = 940;
            $coordY = 1740;

            $defWidth = 250;
            $defHeight = 100;
            $defratio = 1;

            $width = imagesx($fotoimg);
            $height = imagesy($fotoimg);

            $thumb = imagecreatetruecolor($defWidth, $defHeight);
            imagecopyresampled($thumb, $fotoimg, 0, 0, 0, 0, $defWidth, $defHeight, $width, $height);

            $background = imagecolorallocate($thumb , 0, 0, 0);
            imagecolortransparent($thumb, $background);
            imagealphablending( $thumb, false );
            imagesavealpha( $thumb, true );

            $fotoimg_width = imagesx($thumb);
            $fotoimg_height = imagesy($thumb);

            imagecopymerge($img, $thumb, $coordX, $coordY, 0,0, $fotoimg_width, $fotoimg_height, 100);
        }


        ob_start();
        imagejpeg( $img, NULL, 100 );
        imagedestroy( $img );
        $base64Img = ob_get_clean();

        return 'data:image/jpeg;base64,' . base64_encode( $base64Img );
    }

    private function toRupiah($nominal)
    {
        return number_format($nominal, '0', ',', '.');
    }

    private function getXstart($words, $img, $fontSize, $fontStyle, $offset): int
    {
        $dimensions = imagettfbbox($fontSize, 0, $fontStyle, $words);
        $textWidth = abs($dimensions[4] - $dimensions[0]);
        return imagesx($img) - $textWidth + $offset;
    }

    private function getPphTahunan($karyawan, $pkpSetahun)
    {
        $pph = 0;

        // 0-50
        if ($pkpSetahun > 50000000) {
            $pph += 50000000 * ($karyawan->npwp ? 5 : 6) / 100;
            $pkpSetahun -= 50000000;
        } else {
            $pph += $pkpSetahun * ($karyawan->npwp ? 5 : 6) / 100;

            return $pph;
        }

        if ($pkpSetahun > 250000000) {
            $pph += 250000000 * ($karyawan->npwp ? 15 : 18) / 100;
            $pkpSetahun -= 250000000;
        } else {
            $pph += $pkpSetahun * ($karyawan->npwp ? 15 : 18) / 100;

            return $pph;
        }

        if ($pkpSetahun > 500000000) {
            $pph += 500000000 * ($karyawan->npwp ? 25 : 30) / 100;
            $pkpSetahun -= 500000000;
        } else {
            $pph += $pkpSetahun * ($karyawan->npwp ? 25 : 30) / 100;

            return $pph;
        }

        $pph += $pkpSetahun * ($karyawan->npwp ? 30 : 36) / 100;

        return $pph;
    }

    private function getFirstNpwp($npwp)
    {
        return substr($npwp,0,2) . '.' . substr($npwp,2,3) . '.' .
            substr($npwp,5,3) . '.' .substr($npwp,8,1);
    }

    private function getMiddleNpwp($npwp)
    {
        return substr($npwp,9,3);
    }

    private function getLastNpwp($npwp)
    {
        return substr($npwp,12);
    }
}
