<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Variable;

class ConfigController extends Controller
{
    //
    public function index()
    {
        $config = Variable::all();

        return view('config-variable', compact("config"));
    }

    public function save(Request $request)
    {
        $data = json_decode($request->data);

        foreach($data as $key => $val){
            $config = Variable::find($key);
            $config->value = $val;
            $config->save();
        }

        return json_encode(true);
    }

    public function upload(Request $request)
    {
        if ($request->file('ttd') != null) {
            $file = $request->file('ttd');
            $fileName = 'ttd_bukti_potong_pph.png';
            $file->move('img/', $fileName);

            return redirect()->back()->withStatus('Berhasil upload TTD');
        }

        return redirect()->back();
    }
}
