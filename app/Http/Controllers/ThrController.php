<?php

namespace App\Http\Controllers;

use App\Karyawan;
use App\Payroll;
use App\Pph;
use App\Thr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ThrController extends Controller
{
    public function index()
    {
        $seksi = DB::table('karyawan')->select(DB::raw('DISTINCT seksi'))->get()->toArray();
        return view('thr.index', compact('seksi'));
    }

    public function getData(Request $request)
    {
        $wheres = [];
        $tglThr = new Carbon();

        if ($request->seksi != null) {
            $wheres['karyawan.seksi'] = $request->seksi;
        }

        if ($request->id_jabatan != null) {
            $wheres['karyawan.id_jabatan'] = $request->id_jabatan;
        }

        if ($request->tanggal_thr != null) {
            $tglThr = Carbon::createFromFormat('d/m/Y', $request->tanggal_thr);
        }

        if (count($wheres) > 0) {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->leftJoin('thr', 'karyawan.id_karyawan', '=', 'thr.id_karyawan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'),
                         'thr.thr_netto', 'thr.pph')
                ->where($wheres);
        } else {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->leftJoin('thr', 'karyawan.id_karyawan', '=', 'thr.id_karyawan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'),
                         'thr.thr_netto', 'thr.pph');
        }

        return Datatables::of($query)
            ->addIndexColumn()
            ->addColumn('jabatan', function ($karyawan) {
                if ($karyawan->id_jabatan !== NULL) {
                    return $karyawan->nama_jabatan;
                } else {
                    return "Tidak ada jabatan";
                }
            })
            ->addColumn('tunjangan_jabatan', function ($karyawan) {
                $k = Karyawan::find($karyawan->id_karyawan);
                if ($k) {
                    return $karyawan->tunjangan_jabatan;
                }
            })
            ->editColumn('tgl_masuk', '{{ Carbon\Carbon::parse($tgl_masuk)->format("d M Y") }}')
            ->addColumn('thr', function ($karyawan) use ($tglThr) {
                if ($karyawan->thr_netto === null) {
                    return "Belum dihitung";
                }

                $bulanBekerja = $tglThr->diffInMonths($karyawan->tgl_masuk);

                if ($bulanBekerja > 12) {
                    $bulanBekerja = 12;
                }

                return 'Rp ' . number_format($karyawan->thr_netto - $karyawan->pph,2,',','.') . " ($bulanBekerja bln)";
            })
            ->filterColumn('jabatan', function ($query, $keyword) {
                $sql = "jabatan.nama like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('jabatan', function ($query, $order) {
                $query->orderBy('jabatan.nama', $order);
            })
            ->toJson();
    }

    public function hitung(Request $request) {
        $tglThr = Carbon::createFromFormat('d/m/Y', $request->tanggal_thr);
        $tahun = $tglThr->year;
        $bulan = $tglThr->month;

        $karyawans = Karyawan::where(['tgl_resign' => null, 'deleted_at' => null])->get();

        foreach ($karyawans as $karyawan) {
            $thr = Thr::where('id_karyawan', $karyawan->id_karyawan)->whereYear('tgl_thr', $tahun)->first();

            if (!$thr) {
                $thr = new Thr();
                $thr->id_karyawan = $karyawan->id_karyawan;
            }

            $gaji = $karyawan->gaji_pokok + $karyawan->jabatan->tunjangan_jabatan;
            $thr->tgl_thr = $tglThr->format('Y-m-d');

            $payrolls = Payroll::where('id_karyawan', $karyawan->id_karyawan)
                ->where('tahun', $tahun)
                ->where('bulan', '<=', $bulan)
                ->orderBy('bulan', 'asc')
                ->get();

            $gajiSetahun = 0;
            $counter = 12;
            $tempGaji = 0;
            $pphTerbayar = 0;
            $bulanBekerja = $tglThr->diffInMonths($karyawan->carbon_tgl_masuk);

            foreach ($payrolls as $payroll) {
                $tempGaji = $payroll->gaji_pokok + $payroll->tunjangan_jabatan;
                $gajiSetahun += $tempGaji;
                $pphTerbayar += $payroll->pph;

                $counter--;
            }

            if ($counter < 12) {
                $gajiSetahun += $tempGaji * $counter;
            } else {
                $gajiSetahun = $gaji * 12;
            }

            if ($bulanBekerja > 12) {
                $bulanBekerja = 12;
            }

            $nominalThr = $gaji * ($bulanBekerja/12);
            $thr->thr_bruto = $nominalThr;
            $thr->thr_netto = $nominalThr - $nominalThr * (5/100);
            $thr->gaji_pokok_tahun = $gajiSetahun;

            // Perhitungan PPh21
            $gajiThr = (int)($gajiSetahun + $thr->thr_netto);
            $pkp = $gajiThr - (int)$karyawan->ptkp;

            $excess = $pkp % 1000;
            $pkp -= $excess;

            $pph = 0;

            if ($pkp < 0) {
                $thr->pph = $pph;
                $thr->save();
                continue;
            }

            // 0-50
            if ($pkp > 50000000) {
                $pph += 50000000 * ($karyawan->npwp ? 5 : 6) / 100;
                $pkp -= 50000000;
            } else {
                $pph += $pkp * ($karyawan->npwp ? 5 : 6) / 100;

                $thr->pph = $pph - $pphTerbayar;
                $thr->save();
                continue;
            }

            if ($pkp > 250000000) {
                $pph += 250000000 * ($karyawan->npwp ? 15 : 18) / 100;
                $pkp -= 250000000;
            } else {
                $pph += $pkp * ($karyawan->npwp ? 15 : 18) / 100;

                $thr->pph = $pph - $pphTerbayar;
                $thr->save();
                continue;
            }

            if ($pkp > 500000000) {
                $pph += 500000000 * ($karyawan->npwp ? 25 : 30) / 100;
                $pkp -= 500000000;
            } else {
                $pph += $pkp * ($karyawan->npwp ? 25 : 30) / 100;

                $thr->pph = $pph - $pphTerbayar;
                $thr->save();
                continue;
            }

            $pph += $pkp * ($karyawan->npwp ? 30 : 36) / 100;

            $thr->pph = $pph - $pphTerbayar;
            $thr->save();
        }
    }
}
