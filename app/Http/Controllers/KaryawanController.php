<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\Karyawan;
use App\WebToto;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yajra\Datatables\Datatables;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        session(['prev-karyawan' => request()->path()]);
        $seksi = DB::table('karyawan')->select(DB::raw('DISTINCT seksi'))->get()->toArray();
        // return var_dump($seksi);
        return view('karyawan.index', compact('seksi'));
    }

    public function sync()
    {
        $webToto = new WebToto();
        $this->syncAllJabatan($webToto);
        $this->syncAllKaryawan($webToto);
    }

    private function syncAllJabatan(WebToto $webToto)
    {
        $real_jabatan = $webToto->getAllJabatan();
        // if($real_jabatan) return;

        $akuntansi_jabatan = Jabatan::all();
        $pointer = 0;

        // dd($akuntansi_jabatan, $real_jabatan);
        $length_real_jabatan = count($real_jabatan);
        foreach ($akuntansi_jabatan as $a_j) {
            while ($pointer < $length_real_jabatan
                && strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == 1) {

                //tambah;
                $this->syncJabatan($real_jabatan[$pointer]);
                $pointer++;
            }
            if ($pointer >= $length_real_jabatan
                || strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == -1) {
                //hapus;
            } else if (strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == 0) {
                //check;
                //echo "cheking ".$a_j->nama."<br>";
                if ($a_j->nama != $real_jabatan[$pointer]->nama) {

                    //echo "updating ".$a_j->nama."<br>";
                    $this->syncJabatan($real_jabatan[$pointer]);
                }
                $pointer++;
            }
        }
        while ($pointer < $length_real_jabatan) {
            //tambah;
            $this->syncJabatan($real_jabatan[$pointer]);
            $pointer++;
        }
    }

    private function syncJabatan($tmp_jabatan, $new_jabatan = null)
    {
        if ($new_jabatan == null) $new_jabatan = new Jabatan;
        $new_jabatan->id_jabatan = $tmp_jabatan->id_jabatan;
        $new_jabatan->nama = $tmp_jabatan->nama;
        $new_jabatan->save();
    }

    private function syncAllKaryawan(WebToto $webToto)
    {
        $karyawan_hris = $webToto->getAllKaryawan();

        $akuntansi_karyawan = Karyawan::orderBy('id_karyawan', 'asc')->get();
        $pointer = 0;
        $length_karyawan_hris = count($karyawan_hris);

        foreach ($akuntansi_karyawan as $a_k) {
            while ($pointer < $length_karyawan_hris
                && $a_k->id_karyawan > $karyawan_hris[$pointer]->id_karyawan) {
                // tambah;
                $this->syncKaryawan($karyawan_hris[$pointer]);
                $pointer++;
            }

            if ($pointer < $length_karyawan_hris && $a_k->id_karyawan == $karyawan_hris[$pointer]->id_karyawan) {
                // check;
                if ($a_k->updated_at != $karyawan_hris[$pointer]->updated_at) {
                    // update
                    $this->syncKaryawan($karyawan_hris[$pointer], $a_k);
                }
                $pointer++;
            } else if ($a_k->id_karyawan < $karyawan_hris[$pointer]) {
                //hapus;
                $a_k->delete();
            }
        }

        while ($pointer < $length_karyawan_hris) {
            //tambah;
            $this->syncKaryawan($karyawan_hris[$pointer]);
            $pointer++;
        }
    }

    private function syncKaryawan($tmp_karyawan, $a_k = null)
    {
        if ($a_k == null) {
            $a_k = new Karyawan;
        }

        $a_k->id_karyawan = $tmp_karyawan->id_karyawan;
        $a_k->prn = $tmp_karyawan->prn;
        $a_k->nama = $tmp_karyawan->nama;
        $a_k->email = $tmp_karyawan->email;
        $a_k->npwp = $tmp_karyawan->no_npwp;
        $a_k->nik = $tmp_karyawan->no_ktp;
        $a_k->alamat = $tmp_karyawan->alamat_ktp;
        $a_k->jenis_kelamin = $tmp_karyawan->jenis_kelamin;
        $a_k->status_kawin = $tmp_karyawan->status_kawin;
        $a_k->id_jabatan = $tmp_karyawan->id_jabatan;
        $a_k->seksi = $tmp_karyawan->seksi;
        $a_k->tgl_masuk = date('d/m/Y', strtotime($tmp_karyawan->tgl_masuk));
        $a_k->ptkp = $tmp_karyawan->ptkp;
        $a_k->status = $tmp_karyawan->tgl_resign === null ? 'Aktif' : 'Resign';
        $a_k->tgl_resign = $tmp_karyawan->tgl_resign ?? null;
        $a_k->created_at = $tmp_karyawan->created_at;
        $a_k->updated_at = $tmp_karyawan->updated_at;
        $a_k->save();
    }

    public function getData(Request $request)
    {
        $wheres = [];

        if ($request->seksi != null) {
            $wheres['karyawan.seksi'] = $request->seksi;
        }

        if ($request->id_jabatan != null) {
            $wheres['karyawan.id_jabatan'] = $request->id_jabatan;
        }

        if (count($wheres) > 0) {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'))
                ->where($wheres);
        } else {
            $query = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'));
        }

        return Datatables::of($query)
            ->addIndexColumn()
            ->addColumn('jabatan', function ($karyawan) {
                if ($karyawan->id_jabatan !== NULL) {
                    return $karyawan->nama_jabatan;
                } else {
                    return "Tidak ada jabatan";
                }
            })
            ->editColumn('gaji_pokok', '{!! "Rp" . number_format($gaji_pokok,2,",",".") !!}')
            ->addColumn('action', function ($karyawan) {
                return view('karyawan.buttons', compact('karyawan'));
            })
            ->rawColumns(['action'])
            ->filterColumn('jabatan', function ($query, $keyword) {
                $sql = "jabatan.nama like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('jabatan', function ($query, $order) {
                $query->orderBy('jabatan.nama', $order);
            })
            ->toJson();
    }

    public function generateExcel(Request $request)
    {
        $wheres = [];

        if ($request->seksi != null) {
            $wheres['karyawan.seksi'] = $request->seksi;
        }

        if ($request->id_jabatan != null) {
            $wheres['karyawan.id_jabatan'] = $request->id_jabatan;
        }

        if (count($wheres) > 0) {
            $karyawan = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'))
                ->where($wheres)
                ->get();
        } else {
            $karyawan = DB::table('karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->select('karyawan.*', DB::raw('jabatan.nama as nama_jabatan'))
                ->get();
        }

        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();
        $data = [];

        foreach ($karyawan as $k) {
            $data[] = [
                $k->nama,
                $k->prn,
                $k->seksi
            ];
        }

        $sheet->fromArray(
            $data,
            null,
            'A3'
        );

        $writer = new Xlsx($spreadsheet);
        $writer->save("./excel/data_karyawan_SPN.xlsx");

        return dd($karyawan);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Karyawan $karyawan
     *
     * @return Response
     */
    public function show(Karyawan $karyawan)
    {
        return json_encode($karyawan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Karyawan $karyawan
     *
     * @return Response
     */
    public function edit(Karyawan $karyawan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Karyawan $karyawan
     *
     * @return Response
     */
    public function update(Request $request, Karyawan $karyawan)
    {
        $karyawan->update($request->all());

        return 'Berhasil mengupdate data karyawan!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Karyawan $karyawan
     *
     * @return Response
     */
    public function destroy(Karyawan $karyawan)
    {
        //
    }

}
