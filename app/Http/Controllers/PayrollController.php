<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\Karyawan;
use App\Payroll;
use App\Pph;
use App\Services\PphService;
use App\Variable;
use App\WebToto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PDF;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payroll.index');
    }

    public function getData(Request $request)
    {
        $jabatan = Jabatan::find($request->jabatan);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $wheres = [];

        if ($bulan != null && $bulan != "") {
            $wheres['bulan'] = $bulan;
        } else {
            $wheres['bulan'] = date('m');
        }

        if ($tahun != null && $tahun != "") {
            $wheres['tahun'] = $tahun;
        } else {
            $wheres['tahun'] = date('Y');
        }

        if ($jabatan != null && $jabatan != "") {
            $query = DB::table('payroll')
                ->join('karyawan', function ($join) use ($jabatan) {
                    $join->on('payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                        ->where('karyawan.id_jabatan', $jabatan->id_jabatan);
                })
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->select('payroll.*', 'karyawan.prn', 'karyawan.nama', 'karyawan.email', DB::raw('jabatan.nama as jabatan'));
        } else {
            $query = DB::table('payroll')
                ->join('karyawan', 'payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->select('payroll.*', 'karyawan.prn', 'karyawan.nama', 'karyawan.email', DB::raw('jabatan.nama as jabatan'));
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->editColumn('bulan', function ($payroll) {
                $bulan = Carbon::createFromFormat('m', $payroll->bulan)->format('M');
                return "$bulan $payroll->tahun";
            })
            ->editColumn('total_gaji', '{!! "Rp" . number_format($total_gaji,2,",",".") !!}')
            ->addColumn('action', function ($payroll) {
                return view('payroll.buttons', compact('payroll'));
            })
            ->rawColumns(['action'])
            ->filterColumn('prn', function ($query, $keyword) {
                $sql = "karyawan.prn like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('nama', function ($query, $keyword) {
                $sql = "karyawan.nama like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('jabatan', function ($query, $keyword) {
                $sql = "jabatan.nama like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('bulan', function ($query, $keyword) {
                $sql = "DATE_FORMAT(CONCAT(payroll.tahun,'-',payroll.bulan,'-','1'), '%b %Y') like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->orderColumn('jabatan', function ($query, $order) {
                $query->orderBy('jabatan.nama', $order);
            })
            ->toJson();
    }

    public function getLainLain($id_payroll)
    {
        $payroll = Payroll::find($id_payroll);

        if ($payroll == null) {
            return json_encode(null);
        } else {
            return json_encode($payroll);
        }
    }

    public function setLainLain(Request $request)
    {
        $payroll = Payroll::find($request->id_payroll);
        $payroll->update($request->all());
    }

    public function getListPrn(Request $request)
    {
        if ($request->jabatan !== null) {
            return json_encode(Karyawan::select('prn')
                                   ->where('id_jabatan', $request->jabatan)
                                   ->orderBy('prn', 'asc')->get());
        } else {
            return json_encode(Karyawan::select('prn')->orderBy('prn', 'asc')->get());
        }
    }

    public function syncPayroll(Request $request)
    {
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::where('prn', $request->prn)->first();
        $jabatan = $karyawan->jabatan;
        $uangLembur4Jam = $jabatan->lembur_4jam;
        $uangLembur8Jam = $jabatan->lembur_8jam;
        $uangMakan = Variable::getValue('uang_makan');
        $uangPanggilan = Variable::getValue('uang_panggilan');
        $response = (new WebToto())->getPayroll($request->prn, $request->bulan, $request->tahun);

        if ($response === null) {
            return null;
        }

        $potonganAbsensi = (1 / 173) * $response->absensi_jam * $karyawan->gaji_pokok;
        $isidental = $response->isidental * $uangPanggilan; // + uang  makan
        if ($response->hari_kerja > 0) {
            $shift = ($response->shift_hari / $response->hari_kerja) * $jabatan->tunjangan_shift;
        } else {
            $shift = 0;
        }
        $lembur = ($response->lembur_4jam * $uangLembur4Jam) + ($response->lembur_8jam * $uangLembur8Jam);
        $makan = $response->uang_makan;
        $ketIsidental = $response->isidental . ' hari';
        $ketSakit = $response->jumlah_sakit . ' hari';
        $ketIzin = $response->jumlah_izin . ' hari';
        $ketAbsen = $response->jumlah_absen . ' hari';
        $ketLembur4jam = $response->lembur_4jam . ' kali';
        $ketLembur8jam = $response->lembur_8jam . ' kali';

        $ketLain = "";
        $tambahanAbsen = 0;
        $tambahanLain = 0;

        if($response->kelebihan_hari_kerja) { // Kelebihan hari kerja = 8 jam lembur
            $tambahanAbsen += $response->kelebihan_hari_kerja * ($uangLembur4Jam * 2);
            $ketLain .= $response->kelebihan_hari_kerja_keterangan . '; ';
        }

        if ($response->tambahan_jam_kerja) { // Tambahan jam kerja = absensi (1/173)
            $tambahanAbsen += (1 / 173) * $response->tambahan_jam_kerja * $karyawan->gaji_pokok;
            $ketLain .= $response->tambahan_jam_kerja_keterangan . '; ';
        }

        if ($response->tambahan_lain_lain) { // nominal
            $tambahanLain += $response->tambahan_lain_lain;
            $ketLain .= $response->tambahan_lain_lain_keterangan . '; ';
        }

        if ($response->cuti_berbayar) { // Cuti berbayar = gaji pokok/21
            $tambahanAbsen += $response->cuti_berbayar * ($karyawan->gaji_pokok / 21);
            $ketLain .= 'cuti berbayar;';
        }

        $karyawan->sisa_cuti = $response->sisa_cuti;
        $karyawan->save();

        $payroll = Payroll::where(['id_karyawan' => $karyawan->id_karyawan, 'bulan' => $request->bulan, 'tahun' => $request->tahun])->first();
        $totalGaji = $karyawan->gaji_pokok
            + $karyawan->tunjangan_jabatan
            + $shift
            + $lembur
            + $isidental
            + $makan
            - $potonganAbsensi
            + $tambahanAbsen
            + $tambahanLain;

        if ($payroll) {
            $payroll->gaji_pokok = $karyawan->gaji_pokok;
            $payroll->tunjangan_jabatan = $karyawan->tunjangan_jabatan;
            $payroll->absen = $potonganAbsensi;
            $payroll->shift = $shift;
            $payroll->lembur = $lembur;
            $payroll->uang_makan = $makan;
            $payroll->isidental = $isidental;
            $payroll->total_gaji = $totalGaji;
            $payroll->ket_absen = $ketAbsen;
            $payroll->ket_cuti = $response->tgl_cuti;
            $payroll->ket_isidental = $ketIsidental;
            $payroll->ket_izin = $ketIzin;
            $payroll->ket_lembur_4jam = $ketLembur4jam;
            $payroll->ket_lembur_8jam = $ketLembur8jam;
            $payroll->ket_sakit = $ketSakit;
            $payroll->tambahan_lain_lain = $tambahanLain;
            $payroll->ket_lain_lain = $ketLain;
            $payroll->save();
        } else {
            $payroll = Payroll::create(
                [
                    'id_karyawan' => $karyawan->id_karyawan,
                    'bulan' => $request->bulan,
                    'tahun' => $request->tahun,
                    'gaji_pokok' => $karyawan->gaji_pokok,
                    'tunjangan_jabatan' => $karyawan->tunjangan_jabatan,
                    'absen' => $potonganAbsensi,
                    'isidental' => $isidental,
                    'shift' => $shift,
                    'lembur' => $lembur,
                    'uang_makan' => $makan,
                    'total_gaji' => $totalGaji,
                    'ket_absen' => $ketAbsen,
                    'ket_cuti' => $response->tgl_cuti,
                    'ket_isidental' => $ketIsidental,
                    'ket_izin' => $ketIzin,
                    'ket_lembur_4jam' => $ketLembur4jam,
                    'ket_lembur_8jam' => $ketLembur8jam,
                    'ket_sakit' => $ketSakit,
                    'tambahan_lain_lain' => $tambahanLain,
                    'ket_lain_lain' => $ketLain
                ]
            );
        }

        Pph::where('id_payroll', $payroll->id_payroll)->delete();

        $pphService = new PphService($payroll);
        $pphService->getValue();

        return json_encode($payroll);
    }

    public function generatePdf(Request $request)
    {
        $jabatan = Jabatan::find($request->jabatan);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $wheres = [];
        $data = [];

        if ($bulan != null && $bulan != "") {
            $wheres['bulan'] = $bulan;
        }

        if ($tahun != null && $tahun != "") {
            $wheres['tahun'] = $tahun;
        }

        if ($jabatan != null && $jabatan != "") {
            $payrolls = DB::table('payroll')
                ->join('karyawan', function ($join) use ($jabatan) {
                    $join->on('payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                        ->where('karyawan.id_jabatan', $jabatan->id_jabatan);
                })
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->select('payroll.id_payroll')
                ->get();
        } else {
            $payrolls = DB::table('payroll')
                ->join('karyawan', 'payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->select('payroll.id_payroll')
                ->get();
        }

        foreach ($payrolls as $resPayroll) {
            /** @var Payroll $payroll */
            $payroll = Payroll::find($resPayroll->id_payroll);
            /** @var Karyawan $karyawan */
            $karyawan = Karyawan::find($payroll->id_karyawan);
            /** @var Jabatan $jabatan */
            $jabatan = Jabatan::find($karyawan->id_jabatan);

            $data[] = $this->getPayrollDetail($payroll, $karyawan, $jabatan);
        }

        $pdf = PDF::loadView('payroll.slip', ['payroll' => $data]);
        return $pdf->stream("Slip Gaji.pdf");
    }

    public function generateSinglePdf($id_payroll)
    {
        /** @var Payroll $payroll */
        $payroll = Payroll::find($id_payroll);
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($payroll->id_karyawan);
        /** @var Jabatan $jabatan */
        $jabatan = Jabatan::find($karyawan->id_jabatan);

        $data[] = $this->getPayrollDetail($payroll, $karyawan, $jabatan);

        ini_set('max_execution_time', '300');
        ini_set("pcre.backtrack_limit", "5000000");

    //    return view('payroll.slip', ['payroll' => $data]);
        $pdf = PDF::loadView('payroll.slip', ['payroll' => $data]);
        return $pdf->stream("Slip Gaji.pdf");
    }

    public function sendMultiEmail(Request $request)
    {
        $jabatan = Jabatan::find($request->jabatan);
        $bulan = $request->bulan;
        $tahun = $request->tahun;
        $wheres = [];

        if ($bulan != null && $bulan != "") {
            $wheres['bulan'] = $bulan;
        }

        if ($tahun != null && $tahun != "") {
            $wheres['tahun'] = $tahun;
        }

        if ($jabatan != null && $jabatan != "") {
            $payrolls = DB::table('payroll')
                ->join('karyawan', function ($join) use ($jabatan) {
                    $join->on('payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                        ->where('karyawan.id_jabatan', $jabatan->id_jabatan);
                })
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->whereNotNull('karyawan.email')
                ->select('payroll.id_payroll')
                ->get();
        } else {
            $payrolls = DB::table('payroll')
                ->join('karyawan', 'payroll.id_karyawan', '=', 'karyawan.id_karyawan')
                ->join('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id_jabatan')
                ->where($wheres)
                ->whereNotNull('karyawan.email')
                ->select('payroll.id_payroll')
                ->get();
        }

        foreach ($payrolls as $resPayroll) {
            $this->send($resPayroll->id_payroll, $request->password);
        }
    }

    public function sendEmail(Request $request)
    {
        $this->send($request->id_payroll, $request->password);
    }

    private function send($idPayroll, $password)
    {
        /** @var Payroll $payroll */
        $payroll = Payroll::find($idPayroll);
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($payroll->id_karyawan);
        /** @var Jabatan $jabatan */
        $jabatan = Jabatan::find($karyawan->id_jabatan);

        $data = $this->getPayrollDetail($payroll, $karyawan, $jabatan);

        $pdf = PDF::loadView('payroll.email', compact('data'));
        $d["email"] = $karyawan->email;
//        $d["email"] = 'awwabi@its.ac.id';
        $d["client_name"] = $karyawan->nama;
        $d["subject"] = Variable::getValue('subject_email');
        $month = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'];
        $tanggal = $month[$payroll->bulan] . ' ' . $payroll->tahun;

        Config::set('mail.username', Variable::getValue('alamat_email_pengirim'));
        Config::set('mail.password', $password);

        Mail::raw(Variable::getValue('body_email'), function ($message) use ($d, $pdf, $karyawan, $tanggal) {
            $message->from(Variable::getValue('alamat_email_pengirim'), Variable::getValue('nama_email_pengirim'))
                ->to($d["email"], $d["client_name"])
                ->subject($d["subject"])
                ->attachData($pdf->output(), $karyawan->prn . '_' . $karyawan->nama . '_' . $tanggal . '.pdf');
        });

        $payroll->is_sent_email = 1;
        $payroll->save();
    }

    public function formImportTunjangan()
    {
        return view('payroll.import');
    }

    public function importTunjangan(Request $request)
    {
        $tunjanganOnly = isset($request->tunjangan_only);

        if(isset($_FILES['excel'])) {
            $fileName = explode('.', $_FILES['excel']['name']);
            if (end($fileName) === 'xlsx') {
                $reader = new Xlsx();
            } else {
                $reader = new Xls();
            }

            $reader->setReadDataOnly(true);

            $spreadsheet = $reader->load($_FILES['excel']['tmp_name']);
            $sheets = $spreadsheet->getAllSheets();

            $karyawan = Karyawan::select('id_karyawan', 'prn')->get();
            $idKaryawan= [];

            foreach ($karyawan as $item) {
                $idKaryawan[(int)$item->prn] = $item->id_karyawan;
            }

            foreach ($sheets as $sheet) {
                $rows = $sheet->toArray();

                /**
                 * PERIODE[0], PRN[1], KESEHATAN[2], BPJS JHT[3], BPJS JKK[4], BPJS JKM[5], PENSIUN[6], KESEHATAN.P[7],
                 * BPJS JHT.P[8], BPJS JKK.P[9], BPJS JKM.P[10], PENSIUN.P[11], KESEHATAN_[12], JHT_[13], PENSIUN_[14], PENGURANGAN LAIN2[15]
                 */

                foreach ($rows as $row) {
                    if (!isset($idKaryawan[(int)$row[1]])) {
                        continue;
                    }

                    $periode = explode(' ', $row[0]);
                    $bulan = $this->getIndexMonth($periode[0]);
                    $tahun = $periode[1];

                    $payroll = Payroll::where('id_karyawan', $idKaryawan[(int)$row[1]])
                        ->where('bulan', $bulan)
                        ->where('tahun', $tahun)
                        ->first();

                    if ($payroll) {
                        $payroll->bpjs_kesehatan_in = $row[2];
                        $payroll->bpjs_jht_in = $row[3];
                        $payroll->bpjs_jkk_in = $row[4];
                        $payroll->bpjs_jkm_in = $row[5];
                        $payroll->bpjs_jp_in = $row[6];

                        $payroll->bpjs_kesehatan_p_out = $row[7];
                        $payroll->bpjs_jht_p_out = $row[8];
                        $payroll->bpjs_jkk_p_out = $row[9];
                        $payroll->bpjs_jkm_p_out = $row[10];
                        $payroll->bpjs_jp_p_out = $row[11];

                        $payroll->bpjs_kesehatan_out = $row[12];
                        $payroll->bpjs_jht_out = $row[13];
                        $payroll->bpjs_jp_out = $row[14];
                        $payroll->pengurangan_lain_lain = $row[15];

                        $payroll->save();
                    } else {
                        $karyawan = Karyawan::find($idKaryawan[(int)$row[1]]);
                        $payroll = Payroll::create(
                            [
                                'id_karyawan' => $karyawan->id_karyawan,
                                'bulan' => $bulan,
                                'tahun' => $tahun,
                                'gaji_pokok' => $karyawan->gaji_pokok,
                                'tunjangan_jabatan' => $karyawan->tunjangan_jabatan,
                                'bpjs_kesehatan_in' => $row[2],
                                'bpjs_jht_in' => $row[3],
                                'bpjs_jkk_in' => $row[4],
                                'bpjs_jkm_in' => $row[5],
                                'bpjs_jp_in' => $row[6],
                                'bpjs_kesehatan_p_out' => $row[7],
                                'bpjs_jht_p_out' => $row[8],
                                'bpjs_jkk_p_out' => $row[9],
                                'bpjs_jkm_p_out' => $row[10],
                                'bpjs_jp_p_out' => $row[11],
                                'bpjs_kesehatan_out' => $row[12],
                                'bpjs_jht_out' => $row[13],
                                'bpjs_jp_out' => $row[14],
                                'pengurangan_lain_lain' => $row[15],
                            ]
                        );
                    }

                    Pph::where('id_payroll', $payroll->id_payroll)->delete();

                    $pphService = new PphService($payroll);
                    $payroll->tambahan_lain_lain = $pphService->getValue();

                }
            }
        }

        return redirect()->back()->with('success', 'Berhasil mengupdate data tunjangan');
    }

    public function importPph(Request $request)
    {
        if(isset($_FILES['excel'])) {
            $fileName = explode('.', $_FILES['excel']['name']);
            if (end($fileName) === 'xlsx') {
                $reader = new Xlsx();
            } else {
                $reader = new Xls();
            }

            $reader->setReadDataOnly(true);

            $spreadsheet = $reader->load($_FILES['excel']['tmp_name']);
            $sheets = $spreadsheet->getAllSheets();

            $karyawan = Karyawan::select('id_karyawan', 'prn')->get();
            $idKaryawan= [];

            foreach ($karyawan as $item) {
                $idKaryawan[(int)$item->prn] = $item->id_karyawan;
            }

            foreach ($sheets as $sheet) {
                $rows = $sheet->toArray();

                /**
                 * Periode[0], PRN[1], Gaji Pokok[2], Tunj Jabatan[3],
                 * Lembur[4], Shift[5], Absen[6], Kesehatan (P)[7],
                 * JKK (P)[8], JKM (P)[9], JHT (K)[10],
                 * Pensiun (K)[11], PPh[12]
                 */
                foreach ($rows as $row) {
                    if (!isset($idKaryawan[(int)$row[1]])) {
                        continue;
                    }

                    $karyawan = Karyawan::find($idKaryawan[(int)$row[1]]);
                    $periode = explode(' ', $row[0]);
                    $bulan = $this->getIndexMonth($periode[0]);
                    $tahun = $periode[1];

                    $payroll = Payroll::where('id_karyawan', $karyawan->id_karyawan)
                        ->where('bulan', $bulan)
                        ->where('tahun', $tahun)
                        ->first();

                    if ($row[6] < 0) {
                        $absen = ((int)$row[6]) * -1;
                        $tambahanAbsen = 0;
                    } else {
                        $absen = 0;
                        $tambahanAbsen = $row[6];
                    }

                    if ($payroll) {
                        $payroll->gaji_pokok = $row[2];
                        $payroll->tunjangan_jabatan = $row[3];
                        $payroll->lembur = $row[4];
                        $payroll->shift = $row[5];
                        $payroll->absen = $absen;
                        $payroll->tambahan_absen = $tambahanAbsen;
                        $payroll->bpjs_kesehatan_p_out = $row[7];
                        $payroll->bpjs_jkk_p_out = $row[8];
                        $payroll->bpjs_jkm_p_out = $row[9];
                        $payroll->bpjs_jht_out = $row[10];
                        $payroll->bpjs_jp_out = $row[11];
                        $payroll->pph = $row[12];
                        $payroll->save();
                    } else {
                        $payroll = Payroll::create(
                            [
                                'id_karyawan' => $karyawan->id_karyawan,
                                'bulan' => $bulan,
                                'tahun' => $tahun,
                                'gaji_pokok' => $row[2],
                                'tunjangan_jabatan' => $row[6] < 0 ? (int)$row[6] * -1 : 0,
                                'lembur' => $row[4],
                                'shift' => $row[5],
                                'absen' => $absen,
                                'tambahan_absen' => $tambahanAbsen,
                                'bpjs_kesehatan_p_out' => $row[7],
                                'bpjs_jkk_p_out' => $row[8],
                                'bpjs_jkm_p_out' => $row[9],
                                'bpjs_jht_out' => $row[10],
                                'bpjs_jp_out' => $row[11],
                                'pph' => $row[12]
                            ]
                        );
                    }

                    if ($karyawan->gaji_pokok != $row[2]) {
                        $karyawan->gaji_pokok = $row[2];
                        $karyawan->save();
                    }

                    $jabatan = $karyawan->jabatan;

                    if ($karyawan->tunjangan_jabatan != $row[3] || substr($karyawan->prn, 0, 2) != '05') {
                        $jabatan->tunjangan_jabatan = $row[3];
                        $jabatan->save();
                    }

                    Pph::where('id_payroll', $payroll->id_payroll)->delete();

                    $pphService = new PphService($payroll);
                    $pphService->getValue();
                }
            }
        }

        return redirect()->back()->with('success', 'Berhasil mengupdate data pph');
    }

    private function getPayrollDetail(Payroll $payroll, Karyawan $karyawan, Jabatan $jabatan): array
    {
        if ($payroll->pph === null) {
            $pphService = new PphService($payroll);
            $pphBulanan = $pphService->getValue();
        } else {
            $pphBulanan = $payroll->pph;
        }

        $jumlahPlus = $karyawan->gaji_pokok
            + $karyawan->tunjangan_jabatan
            + $payroll->bpjs_kesehatan_in
            + $payroll->bpjs_jht_in
            + $payroll->bpjs_jkk_in
            + $payroll->bpjs_jkm_in
            + $payroll->bpjs_jp_in
            + $payroll->shift
            + $payroll->lembur
            + $payroll->uang_makan
            + $payroll->isidental
            + $payroll->tambahan_absen;
            + $payroll->tambahan_lain_lain;

        $jumlahMinus = $payroll->absen
            + $payroll->bpjs_kesehatan_p_out
            + $payroll->bpjs_jht_p_out
            + $payroll->bpjs_jkk_p_out
            + $payroll->bpjs_jkm_p_out
            + $payroll->bpjs_jp_p_out
            + $payroll->bpjs_kesehatan_out
            + $payroll->bpjs_jht_out
            + $payroll->bpjs_jp_out
            + $payroll->pinjaman
            + $payroll->izin
            + $payroll->pengurangan_lain_lain;

        $total = $jumlahPlus - $jumlahMinus;
        $payroll->total_gaji = $total;
        $payroll->save();

        return [
            'prn_nama' => "$karyawan->prn / $karyawan->nama",
            'jabatan' => $jabatan->nama,
            'seksi' => $karyawan->seksi,
            'sisa_cuti' => $karyawan->sisa_cuti,
            'gaji_pokok' => $payroll->gaji_pokok,
            'tunjangan_jabatan' => $payroll->tunjangan_jabatan,
            'tunjangan_pph' => $pphBulanan,
            'pph' => $pphBulanan,
            'bpjs_kesehatan_in' => $payroll->bpjs_kesehatan_in,
            'bpjs_kesehatan_p_out' => $payroll->bpjs_kesehatan_p_out,
            'bpjs_kesehatan_out' => $payroll->bpjs_kesehatan_out,
            'bpjs_jkk_in' => $payroll->bpjs_jkk_in,
            'bpjs_jkk_p_out' => $payroll->bpjs_jkk_p_out,
            'bpjs_jkm_in' => $payroll->bpjs_jkm_in,
            'bpjs_jkm_p_out' => $payroll->bpjs_jkm_p_out,
            'bpjs_jkm_out' => $payroll->bpjs_jkm_out,
            'bpjs_jht_in' => $payroll->bpjs_jht_in,
            'bpjs_jht_p_out' => $payroll->bpjs_jht_p_out,
            'bpjs_jht_out' => $payroll->bpjs_jht_out,
            'bpjs_jp_in' =>  $payroll->bpjs_jp_in,
            'bpjs_jp_p_out' =>  $payroll->bpjs_jp_p_out,
            'bpjs_jp_out' =>  $payroll->bpjs_jp_out,
            'absensi' => $payroll->absen,
            'izin' => $payroll->izin,
            'lembur' => $payroll->lembur,
            'uang_makan' => $payroll->uang_makan,
            'isidental' => $payroll->isidental,
            'shift' => $payroll->shift,
            'pinjaman' => $payroll->pinjaman,
            'tambahan_lain_lain' => $payroll->tambahan_absen + $payroll->tambahan_lain_lain,
            'pengurangan_lain_lain' => $payroll->pengurangan_lain_lain,
            'jumlah_penambahan' => $jumlahPlus,
            'jumlah_pengurangan' => $jumlahMinus,
            'ket_absen' => $payroll->ket_absen,
            'ket_izin' => $payroll->ket_izin,
            'ket_sakit' => $payroll->ket_sakit,
            'ket_cuti' => $payroll->ket_cuti,
            'ket_lembur_4jam' => $payroll->ket_lembur_4jam,
            'ket_lembur_8jam' => $payroll->ket_lembur_8jam,
            'ket_isidental' => $payroll->ket_isidental,
            'ket_pinjaman' => $payroll->ket_pinjaman,
            'ket_lain_lain' => $payroll->ket_lain_lain,
            'total_gaji' => $total == 0 ? '' : $total
        ];
    }

    protected function getIndexMonth(string $month) {
        $month = ucfirst($month);
        $index = ['Januari' => '01', 'Februari' => '02','Maret' => '03', 'April' => '04', 'Mei' => '05', 'Juni' => '06',
        'Juli' => '07', 'Agustus' => '08', 'September' => '09', 'Oktober' => '10', 'November' => '11', 'Desember' => '12'];

        return $index[ucfirst($month)];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function show(Payroll $payroll)
    {
        Pph::where('id_payroll', $payroll->id_payroll)->delete();

        $pphService = new PphService($payroll);
        $pphService->getValue();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function edit(Payroll $payroll)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payroll $payroll)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payroll  $payroll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payroll $payroll)
    {
        //
    }

    public function test(Request $request)
    {
        foreach (Payroll::where('id_karyawan', 70)->get() as $payroll) {
            Pph::where('id_payroll', $payroll->id_payroll)->delete();

            $pphService = new PphService($payroll);
            $pphService->getValue();
        }
    }
}
