<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        //$this->middleware(['role:super-admin']);
    }
    public function index()
    {
        return view('user.index');

    }
    public function profile()
    {
        return view('user.profile', ["user" => Auth::user()]);
    }
    public function changePassword(Request $request)
    {
        $method = $request->method();
        if ($request->isMethod('put')) {
            $old_password = $request->old_password;
            $new_password = $request->new_password;
            $retype_password = $request->retype_password;
            $user = Auth::user();
            $hasher = app('hash');
            if ($hasher->check($old_password, $user->password)) {
                if (is_string($new_password) && strlen($new_password) > 3) {
                    if ($new_password == $retype_password){
                        $user->password = $new_password;
                        $user->save();
                        $notif = array('success', "Perubahan password berhasil");
                        return redirect()->route('profile')->with('change_pass_notif', $notif);
                    }
                    else {
                        $notif = array('danger', "Pengulangan password tidak sama");
                    }
                }
                else {
                    $notif = array('danger', "Password baru minimal 4 karakter");
                }
            }
            else {
                $notif = array('danger', "Password lama salah");
            }
            return redirect()->route('profile.change-password')->with('change_pass_notif', $notif);
        }
        return view('user.change-password');
    }

    public function getData()
    {
        return Datatables::of(User::query()) //->where('id_seksi', \Auth::user()->id_seksi)
        ->addIndexColumn()
        ->addColumn('action', function(User $user) {
            return view('user.buttons', compact('user'));
        })
        ->rawColumns(['action'])
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create($request->all());

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user_roles = $user->getRoleNames();
        $roles = Role::all();
        return view('user.form', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($request->all()['password']===''){
            $request->all()['password']= $user->password;
        }
        $inputs = $request->all();
        $roles = $request->all()['id_roles'];
        $user->syncRoles($roles);
        unset($inputs['id_roles']);
        $user->update($inputs);

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    }

    public function permission($id_user){
        $user = User::find($id_user);
        $id_permission = [];
        $permissions = $user->getAllPermissions();

        foreach ($permissions as $key => $permission) {
            $id_permission[] = $permission->id;
        }
        return view('user.permission', compact('id_user', 'id_permission'));
        // $user = User::find(38);
        // $permission = Permission::find(3);

        // $user->assignRole('read');
        // $user->removeRole('read');
        // $user->revokePermissionTo('Genta Nuraini');

        // $user->givePermissionTo(4);
        // $user->givePermissionTo(2);
    }

    public function createPermission(Request $request){
        $user = User::find($request->all()['id_user']);

        $permissions = $user->getAllPermissions();

        foreach ($permissions as $key => $permission) {
            $user->revokePermissionTo($permission);
        }

        foreach ($request->all()['permission'] as $key => $id_permission) {
            $permission = Permission::find($id_permission);
            $user->givePermissionTo($permission);
        }
        return redirect()->route('user.index');
    }

    public function role($id_user){
        $user = User::find($id_user);
        $role_name = [];
        $roleNames = $user->getRoleNames();

        foreach ($roleNames as $key => $roleName) {
            $role_name[] = $roleName;
        }
        // print_r($role);die;
        // $user->removeRole();die;
        return view('user.role', compact('id_user', 'role_name'));
        // $user = User::find(25);
        // $user->assignRole('read');
        // $user->removeRole('read');
        // $user->revokePermissionTo('Genta Nuraini');

        // $user->givePermissionTo(4);
        // $user->givePermissionTo(2);
    }

    public function createRole(Request $request){
        // print_r($request->all());

        $user = User::find($request->all()['id_user']);

        $roleNames = $user->getRoleNames();
        foreach ($roleNames as $key => $roleName) {
            $user->removeRole($roleName);
        }

        foreach ($request->all()['role'] as $key => $id_role) {
            $role = Role::find($id_role);
            $user->assignRole($id_role);
        }
        return redirect()->route('user.index');
    }
}
