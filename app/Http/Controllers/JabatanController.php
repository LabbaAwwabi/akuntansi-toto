<?php

namespace App\Http\Controllers;

use App\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jabatan.index');
    }

    public function getData()
    {
        return Datatables::of(Jabatan::query())
        ->addIndexColumn()
        ->editColumn('tunjangan_jabatan', '{!! "Rp" . number_format($tunjangan_jabatan,2,",",".") !!}')
        ->editColumn('tunjangan_shift', '{!! "Rp" . number_format($tunjangan_shift,2,",",".") !!}')
        ->editColumn('lembur_4jam', '{!! "Rp" . number_format($lembur_4jam,2,",",".") !!}')
        ->editColumn('lembur_8jam', '{!! "Rp" . number_format($lembur_8jam,2,",",".") !!}')
        ->addColumn('action',
            function(Jabatan $jabatan) {
                return view('jabatan.buttons', compact('jabatan'));
            }
        )
        ->rawColumns(['action'])
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function show(Jabatan $jabatan)
    {
        return json_encode($jabatan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Jabatan $jabatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jabatan $jabatan)
    {
        $jabatan->update($request->all());

        return "Berhasil update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jabatan $jabatan)
    {
        //
    }
}
