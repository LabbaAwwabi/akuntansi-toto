<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;
use App\Karyawan;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function __construct()
    {
        parent::__construct();
        $this->api_token = null;
        $this->api_host = 'localhost';
        $this->api_port = 3000;
    }

    public function index()
    {
        return view("home");
    }

    public function syncTes()
    {
        $this->syncAllJabatan();
        $this->syncAllKaryawan();
        //return view("home");
    }

    private function syncAllJabatan()
    {
        if($this->api_token == null) {
            $this->api_token = $this->apiLogin('admin@admin.com', 'admin');
        }
        $real_jabatan = $this->getAllJabatan();
        if($real_jabatan) return;
        
        $akuntansi_jabatan = Jabatan::all();
        $pointer = 0;
        $length_real_jabatan = count($real_jabatan);
        
        foreach ($akuntansi_jabatan as $a_j){
            while( $pointer < $length_real_jabatan
                && strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == 1 ) {

                //tambah;
                $this->syncJabatan($real_jabatan[$pointer]);
                $pointer++;
            }
            if(  $pointer >= $length_real_jabatan
              || strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == -1) {
                //hapus;
            }
            else if(strcmp($a_j->id_jabatan, $real_jabatan[$pointer]->id_jabatan) == 0) {
                //check;
                //echo "cheking ".$a_j->nama."<br>";
                if($a_j->nama != $real_jabatan[$pointer]->nama){

                    //echo "updating ".$a_j->nama."<br>";
                    $this->syncJabatan($real_jabatan[$pointer]);
                }
                $pointer++;
            }
        }
        while( $pointer < $length_real_jabatan) {
            //tambah;
            $this->syncJabatan($real_jabatan[$pointer]);
            $pointer++;
        }
        return 404;
    }
    private function syncAllKaryawan()
    {
        if($this->api_token == null) {
            $this->api_token = $this->apiLogin('admin@admin.com', 'admin');
        }
        $real_jabatan = $this->getAllJabatan();
        $akuntansi_jabatan = Jabatan::all();
        $pointer = 0;
        $length_real_jabatan = count($real_jabatan);
        
        $real_karyawan = $this->getAllKaryawan();
        if($real_karyawan) return;
        
        $akuntansi_karyawan = Karyawan::orderBy('prn')->get();
        $pointer = 0;
        $length_real_karyawan = count($real_karyawan);
        
        foreach ($akuntansi_karyawan as $a_k){
            while( $pointer < $length_real_karyawan
                && strcmp($a_k->prn, $real_karyawan[$pointer]->prn) == 1 ) {

                //tambah;
                $this->syncKaryawan($real_karyawan[$pointer]);
                $pointer++;
            }
            if(  $pointer >= $length_real_karyawan
              || strcmp($a_k->prn, $real_karyawan[$pointer]->prn) == -1) {
                //hapus;
            }
            else if(strcmp($a_k->prn, $real_karyawan[$pointer]->prn) == 0) {
                //check;
                //echo "cheking ".$a_k->prn."<br>";
                if($a_k->updated_at != $real_karyawan[$pointer]->updated_at){

                    //echo "updating ".$a_k->prn."<br>";
                    $this->syncKaryawan($real_karyawan[$pointer]);
                }
                $pointer++;
            }
        }
        while( $pointer < $length_real_karyawan) {
            //tambah;
            $this->syncKaryawan($real_karyawan[$pointer]);
            $pointer++;
        }
        return 404;
    }
    private function syncJabatan($tmp_jabatan, $new_jabatan = null) {
        if($new_jabatan == null) $new_jabatan = new Jabatan;
        $new_jabatan->id_jabatan = $tmp_jabatan->id_jabatan;
        $new_jabatan->nama = $tmp_jabatan->nama;
        $new_jabatan->save();
    }
    private function syncKaryawan($tmp_karyawan, $new_karyawan = null) {
        if($new_karyawan == null) $new_karyawan = new Karyawan;
        $new_karyawan->prn = $tmp_karyawan->prn;
        $new_karyawan->nama = $tmp_karyawan->nama;
        $new_karyawan->id_jabatan = $tmp_karyawan->id_jabatan;
        $new_karyawan->tgl_masuk = $tmp_karyawan->tgl_masuk;
        $new_karyawan->created_at = $tmp_karyawan->created_at;
        $new_karyawan->updated_at = $tmp_karyawan->updated_at;
        $new_karyawan->save();
    }
    private function getAllJabatan()
    {
        if($this->api_token != null) {
            $client = new \GuzzleHttp\Client();
            $request = $client->request('GET', 'http://'.$this->api_host.':'.$this->api_port.'/api/jabatan', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                    'Authorization'     => 'Bearer '.$this->api_token,
                ]
            ]);

            $response = json_decode($request->getBody()->getContents());
            return $response;
            //return view('home');
        }
        return null;
    }
    private function getAllKaryawan()
    {
        if($this->api_token != null) {
            $client = new \GuzzleHttp\Client();
            $request = $client->request('GET', 'http://'.$this->api_host.':'.$this->api_port.'/api/karyawan', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                    'Authorization'     => 'Bearer '.$this->api_token,
                ]
            ]);

            $response = json_decode($request->getBody()->getContents());
            return $response;
            //return view('home');
        }
        return null;
    }
    private function apiLogin($email, $password) {
        $client = new \GuzzleHttp\Client();
        try {

            $request = $client->request('POST', 'http://'.$this->api_host.':'.$this->api_port.'/api/login', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Content-type'     => 'application/json',
                ],
                'body' => json_encode([
                    "email" => $email,
                    "password" => $password,
                ])
            ]);
            $response = json_decode($request->getBody()->getContents());
            if(array_key_exists("api_token", $response->data)) {
                return $response->data->api_token;
            };
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            return null;
        }
        return null;
    }
}
