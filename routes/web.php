<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homes', 'HomeController@synctes')->name('homes');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('jabatan/data', 'JabatanController@getData')->name('jabatan.data');
Route::get('karyawan/data', 'KaryawanController@getData')->name('karyawan.data');
Route::get('karyawan/sync', 'KaryawanController@sync')->name('karyawan.sync');
Route::post('karyawan/excel', 'KaryawanController@generateExcel')->name('karyawan.excel');
Route::get('payroll', 'PayrollController@index')->name('payroll');
Route::get('payroll/data', 'PayrollController@getData')->name('payroll.data');
Route::post('payroll/pdf', 'PayrollController@generatePdf')->name('payroll.pdf');
Route::get('payroll/pdf/{id_payroll}', 'PayrollController@generateSinglePdf')->name('payroll.singlepdf');
Route::get('payroll/email', 'PayrollController@sendEmail')->name('payroll.email');
Route::post('payroll/multi-email', 'PayrollController@sendMultiEmail')->name('payroll.multi-email');
Route::post('payroll/prn', 'PayrollController@getListPrn')->name('payroll.prn');
Route::post('payroll/sync', 'PayrollController@syncPayroll')->name('payroll.sync');
Route::put('payroll/lain-lain/save', 'PayrollController@setLainLain')->name('payroll.lain-lain.save');
Route::get('payroll/lain-lain/{id_payroll}', 'PayrollController@getLainLain')->name('payroll.lain-lain');
Route::get('payroll/form-import-tunjangan', 'PayrollController@formImportTunjangan')->name('payroll.form-import-tunjangan');
Route::get('payroll/{payroll}', 'PayrollController@show');
Route::post('payroll/import-tunjangan', 'PayrollController@importTunjangan')->name('payroll.import-tunjangan');
Route::post('payroll/import-pph', 'PayrollController@importPph')->name('payroll.import-pph');
Route::get('pengaturan', 'ConfigController@index')->name('config');
Route::post('pengaturan/save', 'ConfigController@save')->name('config-save');
Route::post('pengaturan/upload', 'ConfigController@upload')->name('config-upload');
Route::get('profile', 'UserController@profile')->name('profile');
Route::put('profile/change-password', 'UserController@changePassword')->name('profile.change-password');
Route::get('thr', 'ThrController@index')->name('thr.index');
Route::get('thr/data', 'ThrController@getData')->name('thr.data');
Route::get('thr/hitung', 'ThrController@hitung')->name('thr.hitung');
Route::get('pph', 'PphController@index')->name('pph.index');
Route::get('pph/data', 'PphController@getData')->name('pph.data');
Route::get('pph/pdf', 'PphController@pdf')->name('pph.pdf');
Route::get('pph/detail', 'PphController@getDetail')->name('pph.detail');
Route::put('pph/update', 'PphController@update')->name('pph.update');

// testing only
Route::get('payroll/test', 'PayrollController@test')->name('payroll.test');

Route::resources([
    'karyawan' => 'KaryawanController',
    'jabatan' => 'JabatanController',
    'user' => 'UserController',
]);
