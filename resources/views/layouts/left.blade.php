@php
    $route = explode('.',Route::currentRouteName())[0];
@endphp
<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item {{ $route === 'dashboard' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('dashboard') }}">
                        <span class="feather-icon"><i data-feather="home"></i></span>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'karyawan' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('karyawan.index') }}">
                        <span class="feather-icon"><i data-feather="users"></i></span>
                        <span class="nav-link-text">Karyawan</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'jabatan' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('jabatan.index') }}">
                        <span class="feather-icon"><i data-feather="award"></i></span>
                        <span class="nav-link-text">Jabatan</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'payroll' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('payroll') }}">
                        <span class="feather-icon"><i data-feather="credit-card"></i></span>
                        <span class="nav-link-text">Payroll</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'thr' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('thr.index') }}">
                        <span class="feather-icon"><i data-feather="gift"></i></span>
                        <span class="nav-link-text">THR</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'pph' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('pph.index') }}">
                        <span class="feather-icon"><i data-feather="file"></i></span>
                        <span class="nav-link-text">PPH 21</span>
                    </a>
                </li>
                <li class="nav-item {{ $route === 'config' ? 'active' : null }}">
                    <a class="nav-link" href="{{ route('config') }}">
                        <span class="feather-icon"><i data-feather="sliders"></i></span>
                        <span class="nav-link-text">Pengaturan</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!-- /Vertical Nav -->
