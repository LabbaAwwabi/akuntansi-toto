@extends('layouts.app')

@section('title', 'Data Thr')

@push('stylesheets')
<!-- Data Table CSS -->
<link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session()->get('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="card">
    <div class="card-header card-header-action">
        <h5>Filter</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-4 col-xs-12">
                <label for="filter-tanggal">Tanggal THR</label>
                <input id="filter-tanggal" type="text" class="form-control single-date-picker" value="{{ date('d/m/Y') }}">
            </div>
            <div class="form-group col-md-4 col-xs-12">
                <label for="filter-seksi">Seksi</label>
                <select id="filter-seksi" class="form-control select2">
                    <option></option>
                    @foreach ($seksi as $item)
                    <option value="{{ $item->seksi }}">{{ $item->seksi }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4 col-xs-12">
                <label for="filter-jabatan">Jabatan</label>
                <select id="filter-jabatan" class="form-control select2">
                    <option></option>
                    @foreach (\App\Jabatan::all() as $item)
                    <option value="{{ $item->id_jabatan }}">{{ " $item->nama" }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12 text-center">
                <button id="btn-filter" class="btn btn-sm btn-outline-success btn-rounded w-20"><i class="fa fa-filter"></i> Tampilkan</button>
                <button id="btn-hitung" class="btn btn-sm btn-outline-info btn-rounded w-20"><i class="fa fa-file"></i> Hitung THR</button>
            </div>
        </div>
    </div>
</div>

<section class="card">
    <div class="card-header card-header-action">
        <h5>Data Karyawan</h5>
    </div>
    <div class="card-body table-responsive w-100">
        <table id="mydatatable" class="table w-100 pb-30">
            <thead>
            <tr>
                <th class="no-filter">#</th>
                <th>PRN</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Tgl Masuk</th>
                <th>THR</th>
                {{--<th class="text-center no-filter" style="width: 167px;"></th>--}}
            </tr>
            </thead>
        </table>
    </div>
</section>

@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<script>
    $(document).ready(function() {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var ajaxData = function(d){
            d._token = csrf_token;
            d.tanggal_thr = $('#filter-tanggal').val();
            d.seksi = $('#filter-seksi').val();
            d.id_jabatan = $('#filter-jabatan').val();
        };

        var mytable = myDataTable('#mydatatable', '{!! route('thr.data') !!}', [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'prn', name: 'prn' },
            { data: 'nama', name: 'nama' },
            { data: 'jabatan', name: 'jabatan' },
            { data: 'tgl_masuk', name: 'tgl_masuk' },
            { data: 'thr', name: 'thr' }
        ], [1, "asc"], ajaxData);

        $('.select2').select2({
            allowClear: true,
            placeholder: '--- Pilih ---',
        });

        $('.single-date-picker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            cancelClass: "btn-secondary",
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        $('#btn-filter').on('click', function(e) {
            Swal.fire({
                title: "Please wait",
                text: "Reload data Thr",
                timer: 2000
            });
            Swal.showLoading();
            mytable.ajax.reload(null, false);
        });

        $('#btn-hitung').on('click', function(e) {
            if ($('#filter-tanggal').val() !== "") {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang menghitung Thr',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();

                let url = `{!! url('thr/hitung') !!}?tanggal_thr=${$('#filter-tanggal').val()}`;
                $.ajax({
                    url: url,
                    type: "GET",
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        Swal.close();
                        mytable.ajax.reload(null, false);
                    },
                    error : function(data) {
                        Swal.close();
                        swal({
                            title: "Oops...",
                            text: data.responseJSON.message,
                            icon: "error",
                            timer: 2000
                        });
                    }
                })
            }
        });
    });
</script>
@endpush
