<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLIP GAJI KARYAWAN</title>
    <style>
        body {
            font-family: Arial;
            font-size: 8pt;
        }

        @page {
            header: page-header;
            footer: page-footer;
        }

        table {
            border-collapse: collapse;
        }

        #tb-center td {
            /*width: 25%;*/
            padding-bottom: 5px;
        }

        .custom_table td {
            border-bottom: 1px solid black;
            padding-left: 5px;
            padding-right: 5px;
            text-align: center;
        }
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <h4 style="border-bottom: 1px solid black;">
            SLIP GAJI KARYAWAN
            <br>
            PT. SURYA PRATIWI NUSANTARA
        </h4>
    </htmlpageheader>

    @foreach ($payroll as $i => $data)
        <table style="width:100%; border: 1px solid black;">
            <thead>
                <tr>
                    <th style="width:20%;"></th>
                    <th style="width:3%;"></th>
                    <th style="width:40%;"></th>
                    <th style="width:4%;"></th>
                    <th style="width:15%;"></th>
                    <th style="width:3%;"></th>
                    <th style="width:15%;"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>PRN / Nama</td>
                    <td>:</td>
                    <td>{{ $data['prn_nama'] }}</td>
                    <td></td>
                    <td>Sisa Cuti</td>
                    <td>:</td>
                    <td>{{ $data['sisa_cuti'] }}</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td colspan="5">{{ $data['jabatan'] }}</td>
                </tr>
                <tr>
                    <td>Seksi</td>
                    <td>:</td>
                    <td colspan="5">{{ $data['seksi'] }}</td>
                </tr>
            </tbody>
        </table>

        <table id="tb-center" style="width:100%; border: 1px solid black;">
            <thead>
                <tr>
                    <th style="width: 23%"></th>
                    <th style="width: 7%"></th>
                    <th style="width: 20%"></th>
                    <th style="width: 23%"></th>
                    <th style="width: 7%"></th>
                    <th style="width: 20%"></th>
                </tr>
                <tr>
                    <th colspan="3" style="border: 1px solid black;">Penambahan</th>
                    <th colspan="3" style="border: 1px solid black;">Pengurangan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Gaji Pokok</td>
                    <td style="width: 10px;">Rp</td>
                    <td style="border-right: 1px solid black; text-align: right; padding-right: 8px;">{{ $data['gaji_pokok'] == 0 ? '-' : number_format($data['gaji_pokok'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">Pinjaman</td>
                    <td style="width: 10px;">Rp</td>
                    <td style="text-align: right; padding-right: 8px;"> {{ $data['pinjaman'] == 0 ? '-' : number_format($data['pinjaman'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Tunjangan Jabatan</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['tunjangan_jabatan'] == 0 ? '-' : number_format($data['tunjangan_jabatan'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS Kesehatan(P)</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_kesehatan_p_out'] == 0 ? '-' : number_format($data['bpjs_kesehatan_p_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;"></td>
                    <td style="border-left: 1px solid black;">BPJS JKK(P)</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jkk_p_out'] == 0 ? '-' : number_format($data['bpjs_jkk_p_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>BPJS Kesehatan</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['bpjs_kesehatan_in'] == 0 ? '-' : number_format($data['bpjs_kesehatan_in'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS JKM(P)</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jkm_p_out'] == 0 ? '-' : number_format($data['bpjs_jkm_p_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>BPJS JKK</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['bpjs_jkk_in'] == 0 ? '-' : number_format($data['bpjs_jkk_in'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS JHT(P)</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jht_p_out'] == 0 ? '-' : number_format($data['bpjs_jht_p_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>BPJS JKM</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['bpjs_jkm_in'] == 0 ? '-' : number_format($data['bpjs_jkm_in'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS JP(P)</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jp_p_out'] == 0 ? '-' : number_format($data['bpjs_jp_p_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>BPJS JHT</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['bpjs_jht_in'] == 0 ? '-' : number_format($data['bpjs_jht_in'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS Kesehatan</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_kesehatan_out'] == 0 ? '-' : number_format($data['bpjs_kesehatan_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>BPJS JP</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['bpjs_jp_in'] == 0 ? '-' : number_format($data['bpjs_jp_in'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS JHT</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jht_out'] == 0 ? '-' : number_format($data['bpjs_jht_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Shift</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['shift'] == 0 ? '-' : number_format($data['shift'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">BPJS JP</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['bpjs_jp_out'] == 0 ? '-' : number_format($data['bpjs_jp_out'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Lembur</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['lembur'] == 0 ? '-' : number_format($data['lembur'] + $data['uang_makan'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black;">Izin</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px;">{{ $data['izin'] == 0 && $data['absensi'] == 0 ? '-' : number_format(($data['izin'] + $data['absensi']),2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Isidental</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['isidental'] == 0 ? '-' : number_format($data['isidental'],2,',','.') }}</td>
                    <td style="border-left: 1px solid black; vertical-align:top;">PPH 21</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; vertical-align:top;">{{ $data['pph'] == 0 ? '-' : number_format($data['pph'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Tunjangan PPH 21</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['tunjangan_pph'] == 0 ? '-' : number_format($data['tunjangan_pph'],2,',','.') }}</td>
                    <td rowspan="4" style="border-left: 1px solid black; vertical-align:top;">Lain-lain</td>
                    <td>Rp</td>
                    <td rowspan="4" style=" text-align: right; padding-right: 8px;vertical-align:top;">{{ $data['pengurangan_lain_lain'] == 0 ? '-' : number_format($data['pengurangan_lain_lain'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td>Lain-lain</td>
                    <td>Rp</td>
                    <td style="text-align: right; padding-right: 8px; border-right: 1px solid black;">{{ $data['tambahan_lain_lain'] == 0 ? '-' : number_format($data['tambahan_lain_lain'],2,',','.') }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table style="width:100%; border: 1px solid black;">
            <tbody>
                <tr>
                    <td style="width: 25%; border-bottom: 1px solid grey;">Jumlah</td>
                    <td style="text-align: right; padding-right: 8px; width: 25%; border-bottom: 1px solid grey;">{{ $data['jumlah_penambahan'] == 0 ? '-' : 'Rp' . number_format($data['jumlah_penambahan'],2,',','.') }}</td>
                    <td style="width: 25%; border-bottom: 1px solid grey;"></td>
                    <td style="text-align: right; padding-right: 8px; width: 25%; border-bottom: 1px solid grey;">{{ $data['jumlah_pengurangan'] == 0 ? '-' : 'Rp' . number_format($data['jumlah_pengurangan'],2,',','.') }}</td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%; text-align: center;"><strong>Total: {{ $data['total_gaji'] == 0 ? '-' : 'Rp' . number_format($data['total_gaji'],2,',','.') }}</strong></td>
                </tr>
            </tbody>
        </table>

        <hr>

        <table id="tb-note" style="width: 100%">
            <tbody>
                <tr>
                    <td style="width: 20%">Cuti</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_cuti'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Izin</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ 'Absen ' . $data['ket_absen'] . ' + Izin ' . $data['ket_izin'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Lembur 4 Jam</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_lembur_4jam'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Lembur 8 Jam</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_lembur_8jam'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Lembur Isidental</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_isidental'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Sakit</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_sakit'] }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Lain-lain</td>
                    <td style="width: 3%">:</td>
                    <td style="width: 77%">{{ $data['ket_lain_lain'] }}</td>
                </tr>
            </tbody>
        </table>

        @if ($i < count($payroll) - 1)
            <pagebreak>
        @endif
    @endforeach

    <htmlpagefooter name="page-footer">
        <div style="width:100%; border-top: 1px solid black;">
            <table width="100%">
                <tr>
                    <td width="50%">Slip dibuat pada {DATE j F Y}</td>
                    <td width="50%" style="text-align: right; padding-right: 8px;">Halaman {PAGENO}/{nbpg}</td>
                </tr>
            </table>
        </div>
    </htmlpagefooter>
</body>
</html>
