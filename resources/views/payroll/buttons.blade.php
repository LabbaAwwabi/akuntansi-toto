<div class="button-group">
  <button value="{{ $payroll->id_payroll }}" modal-id-lain-lain="{{ $payroll->id_payroll }}" class="btn btn-success btn-sm btn-icon btn-icon-style-1">
    <span class="btn-icon-wrap"><i class="material-icons">edit</i></span>
  </button>
  <a href="{{ url('payroll/pdf/' . $payroll->id_payroll) }}" target="_blank" class="btn btn-primary btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">picture_as_pdf</i></span></a>
  @if($payroll->email)
    <button id="btn-email" value="{{ $payroll->id_payroll }}" class="btn {{ $payroll->is_sent_email ? 'btn-danger' : 'btn-info' }} btn-sm btn-icon btn-icon-style-1"
            data-toggle="tooltip" title="Email slip gaji {{ $payroll->is_sent_email ? '(ulangi)' : null }}">
        <span class="btn-icon-wrap"><i class="material-icons">email</i></span>
    </button>
  @endif
</div>
