@extends('layouts.app')

@section('title', 'Data Payroll SPN')

@push('stylesheets')
<!-- Data Table CSS -->
<link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="card">
    <div class="card-header card-header-action">
        <h5>Filter</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-6 col-xs-12">
                <label for="filter-jabatan">Jabatan</label>
                <select id="filter-jabatan" class="form-control select2">
                    <option></option>
                    @foreach (\App\Jabatan::all() as $item)
                        <option value="{{ $item->id_jabatan }}">{{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-3 col-xs-12">
                <label for="filter-bulan">Bulan</label>
                <select id="filter-bulan" class="form-control select2">
                    <option></option>
                    @foreach (['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'] as $id => $text)
                        <option value="{{ $id }}" {{ $id == date('m') ? 'selected' : null }}>{{ $text }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-3 col-xs-12">
                <label for="filter-tahun">Tahun</label>
                <select id="filter-tahun" class="form-control select2">
                    <option></option>
                    @for ($i = date('Y'); $i >= 2000; $i--)
                        <option value="{{ $i }}" {{ $i == date('Y') ? 'selected' : null }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-md-12 text-center">
                <button id="btn-filter" class="btn btn-sm btn-outline-success btn-rounded w-30"><i class="fa fa-filter"></i> Filter</button>
            </div>
        </div>
    </div>
</div>
<section class="card">
    <div class="card-header card-header-action">
        <h5>Data Payroll SPN</h5>
        <div class="d-flex align-items-center">
            <button id="btn-sync" class="btn btn-sm btn-success">
                <i class="fa fa-refresh"></i> Sinkronisasi Data
            </button>
            &nbsp;
            <a href="{{ url('payroll/form-import-tunjangan') }}" class="btn btn-sm btn-success">
                <i class="fa fa-file-excel-o"></i> Import Tunjangan
            </a>
            &nbsp;
            <button id="btn-pdf-all" class="btn btn-sm btn-primary">
                <i class="fa fa-file"></i> Generate PDF
            </button>
            &nbsp;
            <button id="btn-email-all" class="btn btn-sm btn-info">
                <i class="fa fa-envelope"></i> Send Emails
            </button>
        </div>
    </div>
    <div class="card-body table-responsive w-100">
        <table id="mydatatable" class="table w-100 pb-30">
            <thead>
                <tr>
                    <th class="no-filter">#</th>
                    <th>PRN</th>
                    <th>Nama</th>
                    <th>Jabatan</th>
                    <th>Bulan</th>
                    <th>Total Gaji</th>
                    <th class="text-center no-filter" style="width: 40px;"></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Lain-Lain</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="lain-lain-form">
                    {{ method_field('PUT') }}
                    @csrf
                    <input type="hidden" name="id_payroll">
                    <input type="hidden" name="total_gaji">
                    <div class="form-group">
                        <label>Pinjaman</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="pinjaman" type="text">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan Pinjaman</label>
                        <textarea class="form-control" name="ket_pinjaman"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Tambahan lain-lain</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="tambahan_lain_lain" aria-label="Amount (to the nearest dollar)" type="text">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan lain-lain</label>
                        <textarea class="form-control" name="ket_lain_lain"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-lain-lain-form" type="button" class="btn btn-info" data-dismiss="modal">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<form id="download_pdf" target="_blank" action="{{ url('payroll/pdf') }}" method="POST">
    @csrf
    <input type="hidden" id="jabatan" name="jabatan" value="">
    <input type="hidden" id="bulan" name="bulan" value="">
    <input type="hidden" id="tahun" name="tahun" value="">
</form>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var selectJabatan = $('#filter-jabatan');
    var selectBulan = $('#filter-bulan');
    var selectTahun = $('#filter-tahun');
    var listPrn = [];
    var duplicatePrn = [];
    var countPrn = 0;
    var oldShift;
    var oldPinjaman;
    var oldTambahan;
    var oldPengurangan;
    var oldTotal;
    var ajaxData = function(d){
        d._token = csrf_token;
        d.jabatan = selectJabatan.val();
        d.bulan = selectBulan.val();
        d.tahun = selectTahun.val();
    };

    var mytable = myDataTable('#mydatatable', '{!! route('payroll.data') !!}', [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'prn', name: 'prn' },
            { data: 'nama', name: 'nama' },
            { data: 'jabatan', name: 'jabatan' },
            { data: 'bulan', name: 'bulan' },
            { data: 'total_gaji', name: 'total_gaji' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ], [1, "asc"], ajaxData);

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    var showLainLainForm = function (id) {
        $.ajax({
            url: "{{ url('payroll') }}"+"/lain-lain/"+id,
            type: "GET",
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang memuat data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                data = JSON.parse(data);
                if (data) {
                	$("#lain-lain-form [name='pinjaman']").val(data["pinjaman"]);
                	$("#lain-lain-form [name='ket_pinjaman']").val(data["ket_pinjaman"]);
                	$("#lain-lain-form [name='tambahan_lain_lain']").val(data["tambahan_lain_lain"]);
                	$("#lain-lain-form [name='ket_lain_lain']").val(data["ket_lain_lain"]);
                	$("#lain-lain-form [name='id_payroll']").val(id);
                    $("#lain-lain-form [name='total_gaji']").val(data["total_gaji"]);
                    oldPinjaman = Number(data["pinjaman"]);
                    oldTambahan = Number(data["tambahan_lain_lain"]);
                    oldTotal = Number(data["total_gaji"]);
                    $("#myModal").modal("show");
                }
                Swal.close();
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };

    var simpanLainLainForm = function () {
    	var myForm = $('#lain-lain-form')[0];
        var formData = new FormData(myForm);

        $.ajax({
            url: "{{ route('payroll.lain-lain.save') }}",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang menyimpan data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                mytable.ajax.reload();
                Swal.close();
                swal({
                    title: "Berhasil mengupdate data!",
                    text: "Data payroll berhasil diupdate",
                    icon: "success",
                    timer: 2000
                });
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };

    $(document).on("click", "[modal-id-lain-lain]", function () {
    	showLainLainForm($(this).attr("modal-id-lain-lain"));
    });

    $("#btn-lain-lain-form").on("click", function () {
        var temp = (oldTotal + oldPinjaman - oldTambahan)
            - Number($("#lain-lain-form [name='pinjaman']").val())
            + Number($("#lain-lain-form [name='tambahan_lain_lain']").val());
        $("#lain-lain-form [name='total_gaji']").val(temp);
        simpanLainLainForm();
    });

    $('#filter-jabatan').on('select2:select', function(e) {
        var data = e.params.data;

        $.ajax({
            url: "{{ route('payroll.prn') }}",
            type: "POST",
            data: {
                '_method': 'POST',
                '_token': csrf_token,
                'jabatan': selectJabatan.val()
            },
            beforeSend: function() {
                listPrn = [];
                duplicatePrn = [];
                countPrn = 0;
            },
            success: function(data) {
                data = JSON.parse(data);

                data.forEach(element => {
                    listPrn.push(element.prn);
                    duplicatePrn.push(element.prn);
                    countPrn++;
                });
            },
        })
    });

    $('#btn-filter').on('click', function(e) {
        mytable.ajax.reload();
    });

    $('#btn-sync').on('click', function(e) {
        if (selectBulan.val() != "" && selectTahun.val() != "") {
            if (selectJabatan.val() != "") {
                swalSync();
            } else {
                $.ajax({
                    url: "{{ route('payroll.prn') }}",
                    type: "POST",
                    data: {
                        '_method': 'POST',
                        '_token': csrf_token
                    },
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Please wait',
                            text: 'Sedang melakukan sinkronisasi data',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                        Swal.showLoading();
                        listPrn = [];
                        duplicatePrn = [];
                        countPrn = 0;
                    },
                    success: function(data) {
                        data = JSON.parse(data);

                        data.forEach(element => {
                            listPrn.push(element.prn);
                            duplicatePrn.push(element.prn);
                            countPrn++;
                        });

                        Swal.close();
                        swalSync();
                    },
                })
            }
        } else {
            swal({
                title: "Invalid Request",
                text: "Silahkan pilih filter periode (bulan dan tahun) terlebih dahulu",
                icon: "error",
                timer: 2000
            });
        }
    });

    function swalSync() {
        swal({
            title: "Apakah Anda yakin?",
            text: "Proses Sinkronisasi akan mensinkronkan data payroll berdasarkan data dari sistem HRD",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willSync) => {
            if (willSync) {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang melakukan sinkronisasi data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
                listPrn = Array.from(duplicatePrn);
                syncing();
            }
        });
    }

    function syncing() {
        // let listPrn = ['00033'];
        $.ajax({
            url: "{{ route('payroll.sync') }}",
            type: "POST",
            data: {
                '_method': 'POST',
                '_token': csrf_token,
                'prn': listPrn.shift(),
                'bulan': selectBulan.val(),
                'tahun': selectTahun.val()
            },
            beforeSend: function() {
                Swal.getContent().textContent = 'Sinkronisasi ' + (countPrn - listPrn.length) + '/' + countPrn + ' payroll';
            },
            success: function(data) {
                if (listPrn.length > 0) {
                    syncing();
                } else {
                    mytable.ajax.reload(null, false);
                    Swal.close();
                    swal({
                        title: "Berhasil Mensinkronkan data!",
                        text: "Data payroll berhasil disinkronisasi",
                        icon: "success",
                        timer: 2000
                    });
                }
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    }

    $('#btn-pdf-all').on('click', function(e) {
        if (selectBulan.val() != "" && selectTahun.val() != "") {
            $("#jabatan").val(selectJabatan.val());
            $("#bulan").val(selectBulan.val());
            $("#tahun").val(selectTahun.val());
            $("#download_pdf").submit();
        } else {
            swal({
                title: "Oops...",
                text: "Silahkan pilih filter periode (bulan dan tahun) terlebih dahulu",
                icon: "warning",
                timer: 2000
            });
        }
    });

    $(document).on("click", "#btn-email-all", function () {
        let bulan = selectBulan.val();
        let tahun = selectTahun.val();
        let jabatan = selectJabatan.val();

        if (bulan !== "" && tahun !== "") {
            if (jabatan === "") {
                jabatanText = 'Semua Jabatan';
            } else {
                jabatanText = selectJabatan.select2('data')[0].text;
            }

            swal({
                title: "Apakah Anda yakin ?",
                text: `Mengirim semua payroll karyawan dengan filter \nJabatan: ${jabatanText}\nBulan: ${selectBulan.select2('data')[0].text} ${tahun}`,
                icon: "info",
                buttons: true,
            })
                .then((willSend) => {
                    if (willSend) {
                        sendMultiEmail(jabatan, bulan, tahun);
                    }
                });
        } else {
            swal({
                title: "Oops...",
                text: "Silahkan pilih filter periode (bulan dan tahun) terlebih dahulu",
                icon: "warning",
                timer: 2000
            });
        }
    });

    $(document).on("click", "#btn-email", function () {
    	sendEmail($(this).attr("value"));
    });

    function confirmSendEmail() {

    }

    async function sendEmail(id) {
        const { value: password } = await Swal.fire({
            title: 'Kirim Email',
            html: 'Email yang dikirim tidak dapat diurungkan',
            input: 'password',
            inputLabel: 'Password',
            inputPlaceholder: 'Enter your password',
            inputAttributes: {
                maxlength: 50,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        });

        if (password) {
            $.ajax({
                url: "{{ url('payroll/email') }}" + "?id_payroll=" + id + "&password=" + password,
                type: "GET",
                beforeSend: function() {
                    Swal.fire({
                        title: 'Please wait',
                        text: 'Sedang mengirim email',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                success: function(data) {
                    Swal.close();
                    mytable.ajax.reload();
                    swal({
                        title: "Berhasil!",
                        text: "Email berhasil terkirim",
                        icon: "success",
                        timer: 2000
                    });
                },
                error : function(data) {
                    Swal.close();
                    swal({
                        title: "Oops...",
                        text: data.responseJSON.message,
                        icon: "error",
                        timer: 2000
                    });
                }
            });
        }
    }

    async function sendMultiEmail(jabatan, bulan, tahun) {
        const { value: password } = await Swal.fire({
            title: 'Kirim Email',
            html: 'Email yang dikirim tidak dapat diurungkan',
            input: 'password',
            inputLabel: 'Password',
            inputPlaceholder: 'Enter your password',
            inputAttributes: {
                maxlength: 50,
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        });

        if (password) {
            $.ajax({
                url: "{{ url('payroll/multi-email') }}",
                type: "POST",
                data: {
                    '_method': 'POST',
                    '_token': csrf_token,
                    'jabatan': jabatan,
                    'bulan': bulan,
                    'tahun': tahun,
                    'password': password
                },
                beforeSend: function () {
                    Swal.fire({
                        title: 'Please wait',
                        text: 'Sedang mengirim semua email',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                success: function (data) {
                    Swal.close();
                    mytable.ajax.reload();
                    swal({
                        title: "Berhasil!",
                        text: "Email berhasil terkirim",
                        icon: "success",
                        timer: 2000
                    });
                },
                error: function (data) {
                    Swal.close();
                    swal({
                        title: "Oops...",
                        text: data.responseJSON.message,
                        icon: "error",
                        timer: 2000
                    });
                }
            });
        }
    }

});
</script>
@endpush
