@extends('layouts.app')

@section('title', 'Import Payroll')

@push('stylesheets')
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<section class="card">
    <div class="card-header card-header-action">
        <h5>Import Tunjangan</h5>
    </div>
    <div class="card-body">
        <form action="{{ url('payroll/import-tunjangan') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="excel">File Excel</label>
                <input type="file" name="excel" id="excel" class="form-control-file">
            </div>
            <div class="form-group form-check">
                <input name="tunjangan_only" type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Tunjangan saja</label>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
        </form>
    </div>
</section>
<section class="card">
    <div class="card-header card-header-action">
        <h5>Import PPh (2021)</h5>
    </div>
    <div class="card-body">
        <form action="{{ url('payroll/import-pph') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="excel">File Excel</label>
                <input type="file" name="excel" id="excel" class="form-control-file">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>
        </form>
    </div>
</section>
@endsection

@push('scripts')
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
</script>
@endpush
