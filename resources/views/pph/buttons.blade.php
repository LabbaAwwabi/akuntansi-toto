<div class="button-group">
    <button id="btn-show" value="{{ $karyawan->id_karyawan }}" class="btn btn-info btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">pageview</i></span></button>
    <button id="btn-edit" value="{{ $karyawan->id_karyawan }}" class="btn btn-success btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">edit</i></span></button>
</div>
