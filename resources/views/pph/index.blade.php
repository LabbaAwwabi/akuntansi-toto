@extends('layouts.app')

@section('title', 'Data PPH 21')

@push('stylesheets')
<!-- Data Table CSS -->
<link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session()->get('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="card">
    <div class="card-header card-header-action">
        <h5>Filter</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-4 col-xs-12">
                <label for="filter-seksi">Seksi</label>
                <select id="filter-seksi" class="form-control select2">
                    <option></option>
                    @foreach ($seksi as $item)
                    <option value="{{ $item->seksi }}">{{ $item->seksi }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4 col-xs-12">
                <label for="filter-jabatan">Jabatan</label>
                <select id="filter-jabatan" class="form-control select2">
                    <option></option>
                    @foreach (\App\Jabatan::all() as $item)
                    <option value="{{ $item->id_jabatan }}">{{ " $item->nama" }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-4">
                <label>Tahun</label>
                <select id="filter-tahun" name="tahun" class="form-control select2">
                    <option></option>
                    @for ($i = date('Y'); $i >= 2000; $i--)
                        <option value="{{ $i }}" {{ $i == date('Y') ? 'selected' : null }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-md-12 text-center">
                <button id="btn-filter" class="btn btn-sm btn-outline-success btn-rounded w-20"><i class="fa fa-filter"></i> Filter</button>
            </div>
        </div>
    </div>
</div>

<section class="card">
    <div class="card-header card-header-action">
        <h5>Data Karyawan</h5>
    </div>
    <div class="card-body table-responsive w-100">
        <table id="mydatatable" class="table w-100 pb-30">
            <thead>
            <tr>
                <th class="no-filter">#</th>
                <th>PRN</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Seksi</th>
                <th>Status</th>
                <th class="text-center no-filter" style="width: 167px;"></th>
            </tr>
            </thead>
        </table>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Edit Bukti PPH</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="edit-form" onsubmit="event.preventDefault();">
                    @csrf
                    {{ method_field('PUT') }}
                    <input type="hidden" name="id_karyawan">
                    <div class="form-group">
                        <label>Nomor</label>
                        <div class="input-group">
                            <input class="form-control" name="nomor" type="number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Penghasilan neto masa sebelumnya</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="perhitungan_13" type="number">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Pph PASAL 21 yang telah dipotong masa sebelumnya</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="perhitungan_18" type="number">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-update" type="button" class="btn btn-success" data-dismiss="modal">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
    $(document).ready(function() {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        var ajaxData = function(d){
            d._token = csrf_token;
            d.seksi = $('#filter-seksi').val();
            d.id_jabatan = $('#filter-jabatan').val();
        };

        var mytable = myDataTable('#mydatatable', '{!! route('pph.data') !!}', [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'prn', name: 'prn' },
            { data: 'nama', name: 'nama' },
            { data: 'jabatan', name: 'jabatan' },
            { data: 'seksi', name: 'seksi' },
            { data: 'status', name: 'status' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ], [1, "asc"], ajaxData);

        $('.select2').select2({
            allowClear: true,
            placeholder: '--- Pilih ---',
        });

        $('#btn-filter').on('click', function(e) {
            mytable.ajax.reload();
        });

        $(document).on("click", "#btn-edit", function () {
            showEditModal($(this).attr("value"));
        });

        $(document).on("click", "#btn-show", function () {
            let baseUrl = '{!! route('pph.pdf') !!}';
            let idKaryawan = $(this).attr("value");
            let tahun = $('#filter-tahun').val();

            window.open(`${baseUrl}?id_karyawan=${idKaryawan}&tahun=${tahun}`);
        });

        $("#btn-update").on("click", function () {
            saveEditModal();
        });

        function showEditModal(id) {
            $.ajax({
                url: "{{ url('pph/detail') }}" + "?id_karyawan=" + id + '&tahun=' + $('#filter-tahun').val(),
                type: "GET",
                beforeSend: function() {
                    Swal.fire({
                        title: 'Please wait',
                        text: 'Sedang memuat data',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data) {
                        $("#edit-form [name='nomor']").val(data["nomor"]);
                        $("#edit-form [name='perhitungan_13']").val(data["perhitungan_13"]);
                        $("#edit-form [name='perhitungan_18']").val(data["perhitungan_18"]);
                        $("#edit-form [name='id_karyawan']").val(id);
                        $("#myModal").modal("show");
                    }
                    Swal.close();
                },
                error : function(data) {
                    Swal.close();
                    swal({
                        title: "Oops...",
                        text: data.responseJSON.message,
                        icon: "error",
                        timer: 2000
                    });
                }
            })
        };

        function saveEditModal() {
            var myForm = $('#edit-form')[0];
            var formData = new FormData(myForm);

            $.ajax({
                url: "{{ url('pph/update') }}" + '?tahun=' + $('#filter-tahun').val(),
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Please wait',
                        text: 'Sedang menyimpan data',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                success: function(data) {
                    // mytable.ajax.reload( null, false );
                    Swal.close();
                    swal({
                        title: "Berhasil mengupdate data!",
                        text: "Data PPH berhasil diupdate",
                        icon: "success",
                        timer: 2000
                    });
                },
                error : function(data) {
                    Swal.close();
                    swal({
                        title: "Oops...",
                        text: data.responseJSON.message,
                        icon: "error",
                        timer: 2000
                    });
                }
            })
        };
    });
</script>
@endpush
