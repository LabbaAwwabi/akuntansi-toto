<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form 1721-A1</title>
</head>
<body style="text-align: center">
    <img src="{{ $data['base64'] }}" alt="form" style="width:100%; height:auto;">
</body>
</html>
