@extends('layouts.app')

@section('title', 'Pengaturan')

@section('content')
<div class="card col-md-8 offset-md-2 col-xs-12">
    <div class="card-header card-header-action">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#nav-api" role="tab">Sinkronisasi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#nav-payroll" role="tab">Payroll</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#nav-pph" role="tab">PPH 21</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#nav-email" role="tab">Email</a>
            </li>
        </ul>
        <div class="d-flex align-items-center">
            <button id="save-config" class="btn btn-sm btn-primary">
                Simpan
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane show active" id="nav-api" role="tabpanel">
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('base_url_api');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('username');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('password');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="password" value="{{ $v->value }}">
                </div>
            </div>
            <div class="tab-pane fade" id="nav-payroll" role="tabpanel">
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('uang_lembur_4jam');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="number" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('uang_makan');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="number" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('uang_panggilan');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="number" value="{{ $v->value }}">
                </div>
            </div>
            <div class="tab-pane fade" id="nav-pph" role="tabpanel">
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('npwp_perusahaan');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="number" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('nama_perusahaan');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('npwp_direktur');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="number" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('nama_direktur');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <form action="{{ route('config-upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <div class="text-center my-3">
                            <img src="{{ asset('img') . '/ttd_bukti_potong_pph.png' }}" alt="Gambar ttd" height="50">
                        </div>
                        <label class="form-control-label">TTD</label>
                        <div class="row">
                            <div class="col-10">
                                <div class="custom-file">
                                    <label class="custom-file-label">Pilih gambar {.png}</label>
                                    <input type="file" name="ttd" class="custom-file-input" accept="image/png">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success col-2">Upload</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_1_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_2_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_3_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_4_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_1_non_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_2_non_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_3_non_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                        <div class="form-group">
                            @php
                                $v = \App\Variable::getVariable('pkp_4_non_npwp');
                            @endphp
                            <label for="{{ $v->id }}">{{ $v->label }}</label>
                            <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-email" role="tabpanel">
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('subject_email');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('body_email');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <textarea class="form-control input-dinamis" name="{{ $v->id }}" type="text">{{ $v->value }}</textarea>
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('nama_email_pengirim');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
                <div class="form-group">
                    @php
                        $v = \App\Variable::getVariable('alamat_email_pengirim');
                    @endphp
                    <label for="{{ $v->id }}">{{ $v->label }}</label>
                    <input class="form-control input-dinamis" name="{{ $v->id }}" type="text" value="{{ $v->value }}">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script type="text/javascript" >
$(function () {
    var input_new = {};
    $(".input-dinamis").change(function () {
        input_new[$(this).attr("name")] = $(this).val();
    });
    var submit_form = function () {
        if (isEmpty(input_new)) return;
        $.ajax({
            type: 'POST',
            url: "{{ route('config-save') }}",
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'data': JSON.stringify(input_new),
            },
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Menyimpan data pengaturan',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                Swal.close();
                swal({
                    title: "Berhasil",
                    text: "Data pengaturan berhasil diupdate",
                    icon: "success",
                    timer: 2000
                });
                data = JSON.parse(data);
                if (data) {
                	input_new = {};
                }
            },
            error: function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        });
    };

    $("#save-config").click(function () {
        submit_form();
    });

    function isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
});
</script>
@endpush
