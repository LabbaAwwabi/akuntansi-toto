@extends('layouts.app')

@push('stylesheets')
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<!-- jquery-steps css -->
<link href="{{ asset('vendors/jquery-steps/demo/css/jquery.steps.css') }}" rel="stylesheet" type="text/css">
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<!-- Bootstrap Dropzone CSS -->
<link href="{{ asset('vendors/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css"/>
@endpush

@isset($karyawan)
    @section('title', 'Ubah Karyawan')

    @section('breadcrumb')
        {{ Breadcrumbs::render('karyawan.edit', $karyawan) }}
    @endsection

    @php
        $action = route('karyawan.update', $karyawan);
        $method = method_field('PUT');
    @endphp
@endisset

@empty($karyawan)
    @section('title', 'Tambah Karyawan')

    @section('breadcrumb')
        {{ Breadcrumbs::render('karyawan.create') }}
    @endsection

    @php
        $karyawan = new \App\Karyawan();
        $action = route('karyawan.store');
        $method = method_field('POST');
    @endphp
@endempty

@section('content')
<form id="myform" action="{{ $action }}" method="POST" enctype="multipart/form-data">
    {{ $method }}
    @csrf
    <div id="step-form">
        <h5>
            <span class="wizard-icon-wrap"><i class="ion ion-md-person"></i></span>
            <span class="wizard-head-text-wrap">
                <span class="step-head">Biodata</span>
            </span>
        </h5>
        <section class="m-0">
            <div class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
                <div class="form-group">
                    <label for="foto">Foto</label>
                    <input name="foto" type="file" id="input-file-now" class="dropify" />
                </div>
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input name="nama" type="text" class="form-control" value="{{ $karyawan->nama }}">
                </div>
                <div class="form-group">
                    <label for="no_ktp">Nomor KTP (NIK)</label>
                    <input name="no_ktp" type="text" class="form-control" value="{{ $karyawan->no_ktp }}">
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label class="control-label mb-10">Jenis Kelamin</label>
                        <div class="pl-3">
                            <div class="custom-radio form-check-inline">
                                <input id="radio_male" value="Laki-laki" name="jenis_kelamin" class="custom-control-input form-check-input"
                                @if ($karyawan->jenis_kelamin != "Perempuan")
                                    checked
                                @endif
                                type="radio">
                                <label for="radio_male" class="custom-control-label">Laki-laki</label>
                            </div>
                            <div class="custom-radio form-check-inline">
                                <input id="radio_female" value="Perempuan" name="jenis_kelamin" class="custom-control-input form-check-input"
                                @if ($karyawan->jenis_kelamin == "Perempuan")
                                    checked
                                @endif
                                type="radio">
                                <label for="radio_female" class="custom-control-label">Perempuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="agama">Agama</label>
                        <select name="agama" class="form-control select2">
                            <option></option>
                            @foreach (["Islam", "Kristen", "Katholik", "Hindu", "Budha", "Konghuchu", "Sinto"] as $item)
                                <option value="{{ $item }}" @if ($karyawan->agama == $item)
                                    selected
                                @endif>{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input name="tempat_lahir" type="text" placeholder="Nama Kabupaten/Kota" class="form-control" value="{{ $karyawan->tempat_lahir }}">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="tgl_lahir">Tanggal Lahir</label>
                        <input name="tgl_lahir" type="text" class="form-control single-date-picker" value="{{ $karyawan->tgl_lahir }}">
                    </div>
                </div>
                <label class="mt-20">Alamat KTP</label>
                <div class="row">
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_ktp_provinsi">Provinsi</label>
                        <select name="alamat_ktp_provinsi" class="form-control select2">
                            <option></option>
                            @foreach (\App\Province::all() as $item)
                                <option value="{{ $item->id }}" @if ($karyawan->alamat_ktp_provinsi == $item->id)
                                    selected
                                @endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_ktp_kabupaten_kota">Kabupaten/Kota</label>
                        <select name="alamat_ktp_kabupaten_kota" class="form-control select2">
                            <option></option>
                            @if ($karyawan->alamat_ktp_provinsi != null)
                                @foreach (\App\Regency::where('province_id', $karyawan->alamat_ktp_provinsi)->get() as $item)
                                    <option value="{{ $item->id }}" @if ($karyawan->alamat_ktp_kabupaten_kota == $item->id)
                                        selected
                                    @endif>{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_ktp_kecamatan">Kecamatan</label>
                        <select name="alamat_ktp_kecamatan" class="form-control select2">
                            <option></option>
                            @if ($karyawan->alamat_ktp_provinsi != null)
                                @foreach (\App\District::where('regency_id', $karyawan->alamat_ktp_kabupaten_kota)->get() as $item)
                                    <option value="{{ $item->id }}" @if ($karyawan->alamat_ktp_kecamatan == $item->id)
                                        selected
                                    @endif>{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <label for="alamat_ktp">Alamat Detail</label>
                        <textarea name="alamat_ktp" class="form-control" rows="2">{{ $karyawan->alamat_ktp }}</textarea>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" id="alamat-domisili-checkbox" type="checkbox">
                    <label class="custom-control-label" for="alamat-domisili-checkbox">Alamat Domisili (centang untuk menduplikasi alamat KTP)</label>
                </div>
                <div class="row" id="wrapper-domisili">
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_domisili_provinsi">Provinsi</label>
                        <select name="alamat_domisili_provinsi" class="form-control select2">
                            <option></option>
                            @foreach (\App\Province::all() as $item)
                                <option value="{{ $item->id }}" @if ($karyawan->alamat_domisili_provinsi == $item->id)
                                    selected
                                @endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_domisili_kabupaten_kota">Kabupaten/Kota</label>
                        <select name="alamat_domisili_kabupaten_kota" class="form-control select2">
                            <option></option>
                            @if ($karyawan->alamat_domisili_provinsi != null)
                                @foreach (\App\Regency::where('province_id', $karyawan->alamat_domisili_provinsi)->get() as $item)
                                    <option value="{{ $item->id }}" @if ($karyawan->alamat_domisili_kabupaten_kota == $item->id)
                                        selected
                                    @endif>{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-xs-12">
                        <label for="alamat_domisili_kecamatan">Kecamatan</label>
                        <select name="alamat_domisili_kecamatan" class="form-control select2">
                            <option></option>
                            @if ($karyawan->alamat_domisili_provinsi != null)
                                @foreach (\App\District::where('regency_id', $karyawan->alamat_domisili_kabupaten_kota)->get() as $item)
                                    <option value="{{ $item->id }}" @if ($karyawan->alamat_domisili_kecamatan == $item->id)
                                        selected
                                    @endif>{{ $item->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <label for="alamat_domisili">Alamat Detail</label>
                        <textarea name="alamat_domisili" class="form-control" rows="2">{{ $karyawan->alamat_domisili }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                    <select name="pendidikan_terakhir" class="form-control select2">
                        <option></option>
                        @foreach (['SMP', 'SMA', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3'] as $item)
                            <option value="{{ $item }}" @if ($karyawan->pendidikan_terakhir == $item)
                                selected
                            @endif>{{ $item }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" value="{{ $karyawan->email }}">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="no_hp">Nomor HP</label>
                        <input name="no_hp" type="text" class="form-control" value="{{ $karyawan->no_hp }}">
                    </div>
                </div>
            </div>
        </section>
        <h5>
            <span class="wizard-icon-wrap"><i class="ion ion-md-business"></i></span>
            <span class="wizard-head-text-wrap">
                <span class="step-head">Data Karyawan</span>
            </span>
        </h5>
        <section class="m-0">
            <div class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="id_status_karyawan">Status Karyawan</label>
                        <select name="id_status_karyawan" class="form-control select2">
                            <option></option>
                            @foreach (\App\StatusKaryawan::all() as $status)
                                <option value="{{ $status->id_status_karyawan }}"
                                    @if ($karyawan->id_status_karyawan == $status->id_status_karyawan)
                                        selected
                                    @endif>
                                    {{ $status->nama }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="prn">PRN</label>
                        <input name="prn" type="text" placeholder="Auto Generate PRN" class="form-control" value="{{ $karyawan->prn }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="id_jabatan">Jabatan</label>
                        <select name="id_jabatan" class="form-control select2">
                            <option></option>
                            @foreach (\App\Jabatan::all() as $jabatan)
                                <option value="{{ $jabatan->id_jabatan }}"
                                    @if ($karyawan->id_jabatan == $jabatan->id_jabatan)
                                        selected
                                    @endif>
                                    {{ $jabatan->nama }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="id_seksi">Seksi</label>
                        <select name="id_seksi" class="form-control select2">
                            <option></option>
                            @foreach (\App\Seksi::all() as $seksi)
                                <option value="{{ $seksi->id_seksi }}"
                                    @if ($karyawan->id_seksi == $seksi->id_seksi)
                                        selected
                                    @endif>
                                    {{ $seksi->nama }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tgl_masuk">Tanggal Masuk</label>
                    <input name="tgl_masuk" type="text" class="form-control single-date-picker" value="{{ $karyawan->tgl_masuk }}">
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="nama_bank">Bank</label>
                        <select name="nama_bank" class="form-control select2">
                            <option></option>
                            @foreach (['BCA', 'BNI', 'BRI', 'Mandiri'] as $bank)
                                <option value="{{ $bank }}"
                                    @if ($karyawan->nama_bank == $bank)
                                        selected
                                    @endif>
                                    {{ $bank }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="no_rekening">Nomor Rekening</label>
                        <input name="no_rekening" type="text" class="form-control" value="{{ $karyawan->no_rekening }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_npwp">Nomor NPWP</label>
                    <input name="no_npwp" type="text" class="form-control" value="{{ $karyawan->no_npwp }}">
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="no_bpjs_kesehatan">Nomor BPJS Kesehatan</label>
                        <input name="no_bpjs_kesehatan" type="text" class="form-control" value="{{ $karyawan->no_bpjs_kesehatan }}">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="no_bpjs_ketenagakerjaan">Nomor BPJS Ketenagakerjaan</label>
                        <input name="no_bpjs_ketenagakerjaan" type="text" class="form-control" value="{{ $karyawan->no_bpjs_ketenagakerjaan }}">
                    </div>
                </div>
            </div>
        </section>
        <h5>
            <span class="wizard-icon-wrap"><i class="ion ion-md-people"></i></span>
            <span class="wizard-head-text-wrap">
                <span class="step-head">Data Keluarga</span>
            </span>
        </h5>
        <section class="m-0">
            <div class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
                <div class="form-group">
                    <label for="status_kawin">Status Kawin</label>
                    <select name="status_kawin" class="form-control select2">
                        <option></option>
                        @foreach (['TK/0', 'TK/1', 'TK/2', 'TK/3', 'K/0', 'K/1', 'K/2', 'K/3'] as $text)
                            <option value="{{ $text }}" @if ($karyawan->status_kawin == $text)
                                selected
                            @endif>{{ $text }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="no_emergency_call">Nomor Emergency Call</label>
                    <input name="no_emergency_call" type="text" class="form-control" value="{{ $karyawan->no_emergency_call }}">
                </div>
                <div class="row">
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="nama_emergency_call">Nama</label>
                        <input name="nama_emergency_call" type="text" class="form-control" value="{{ $karyawan->nama_emergency_call }}">
                    </div>
                    <div class="form-group col-md-6 col-xs-12">
                        <label for="relasi_emergency_call">Relasi</label>
                        <select name="relasi_emergency_call" class="form-control select2">
                            <option></option>
                            @foreach (['Ayah', 'Ibu', 'Istri', 'Suami', 'Anak', 'Kakak', 'Adik', 'Kakek', 'Nenek', 'Paman', 'Bibi', 'Keponakan'] as $item)
                                <option value="{{ $item }}" @if ($karyawan->relasi_emergency_call == $item)
                                    selected
                                @endif>{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>
@endsection

@push('scripts')
<!-- Jasny-bootstrap  JavaScript -->
<script src="{{ asset('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Form Wizard JavaScript -->
<script src="{{ asset('vendors/jquery-steps/build/jquery.steps.min.js') }}"></script>
<!-- Dropify JavaScript -->
<script src="{{ asset('vendors/dropify/dist/js/dropify.min.js') }}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ url('js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\StoreKaryawanRequest', '#myform') !!}
<script>
$(function(){
	"use strict";

	if($('#step-form').length >0) {
		$("#step-form").steps({
            autoFocus: true,
            enableAllSteps: true,
			bodyTag: "section",
			headerTag: "h5",
            titleTemplate: "#title#",
			transitionEffect: "fade",
            onFinished: function (event, currentIndex) {
                var myform = $('#myform');
                var validator = $('#myform').data("validator");

                if (validator) {
                    validator.form();

                    Swal.fire({
                        title: 'Please wait',
                        text: 'Validating form',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    Swal.showLoading();

                    if (myform.valid()) {
                        console.log(myform.serialize());
                        myform.submit();
                    } else {
                        Swal.close();
                        swal({
                            title: "Invalid Input",
                            text: 'Silahkan isi semua form terlebih dahulu',
                            icon: "warning",
                            timer: 1500
                        });
                    }
                }
            },
            labels: {
                cancel: "Batal",
                current: "Form sekarang:",
                pagination: "Pagination",
                finish: "Simpan",
                next: "Berikutnya",
                previous: "Sebelumnya",
                loading: "Loading ..."
            }
        });
    }
});
</script>

<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>

<script>
$(document).ready(function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var inputPrn = $('input[name="prn"]');
    var select2KtpProvinsi = $('select[name="alamat_ktp_provinsi"]');
    var select2KtpKota = $('select[name="alamat_ktp_kabupaten_kota"]');
    var select2KtpKecamatan = $('select[name="alamat_ktp_kecamatan"]');
    var ktpDetail = $('textarea[name="alamat_ktp"]');
    var select2DomisiliProvinsi = $('select[name="alamat_domisili_provinsi"]');
    var select2DomisiliKota = $('select[name="alamat_domisili_kabupaten_kota"]');
    var select2DomisiliKecamatan = $('select[name="alamat_domisili_kecamatan"]');
    var domisiliDetail = $('textarea[name="alamat_domisili"]');
    var select2Status = $('select[name="id_status_karyawan"]');

    var myform = $('#myform');
    var validator = $('#myform').data("validator");

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    $(document).on("change", ".select2", function() {
        validator.element(this);
    });

    select2Status.on('select2:selecting', function (e) {
        inputPrn.val("");
        inputPrn.attr("placeholder", "Loading Data...");
        validator.element(inputPrn);
    });

    select2Status.on('select2:select', function (e) {
        var data = e.params.data;
        var statusKaryawan = @json(\App\StatusKaryawan::all());
        var prn;

        statusKaryawan.forEach(status => {
            if (status.id_status_karyawan == data.id) {
                prn = status.kode;
                return false;
            }
        });

        $.ajax({
            type: 'POST',
            url: "{{ url('karyawan') }}" + '/index-prn',
            data: {
                '_token': csrf_token,
                'kode': prn,
            },
            success: function(data) {
                prn = JSON.parse(data);
                inputPrn.val(prn);
                validator.element(inputPrn);
            }
        });

    });

    select2Status.on('select2:clear', function (e) {
        inputPrn.val("");
        inputPrn.attr("placeholder", "Auto Generate PRN");
        validator.element(inputPrn);
    });

    $('.single-date-picker').daterangepicker({
        singleDatePicker: true,
		showDropdowns: true,
        cancelClass: "btn-secondary",
        locale: {
           format: 'DD/MM/YYYY'
        }
    });

    $('.dropify').dropify();

    select2KtpProvinsi.on('select2:select', function (e) {
        var data = e.params.data;
        var idProvinsi = data.id;

        select2KtpKota.html('').select2({
            allowClear: true,
            placeholder: 'Loading data...',
            data: [{id: '', text: ''}],
            disabled: true
        });

        $.ajax({
            type: 'POST',
            url: "{{ url('karyawan') }}" + '/kota',
            data: {
                '_token': csrf_token,
                'id': data.id,
            },
            success: function(data) {
                data = JSON.parse(data);
                data.unshift({id:'', text:''});

                select2KtpKota.html('').select2({
                    allowClear: true,
                    placeholder: '--- Pilih ---',
                    data: data,
                    disabled: false
                });
            }
        });

    });

    select2KtpKota.on('select2:select', function (e) {
        var data = e.params.data;
        var idKota = data.id;

        select2KtpKecamatan.html('').select2({
            allowClear: true,
            placeholder: 'Loading data...',
            data: [{id: '', text: ''}],
            disabled: true
        });

        $.ajax({
            type: 'POST',
            url: "{{ url('karyawan') }}" + '/kecamatan',
            data: {
                '_token': csrf_token,
                'id': data.id,
            },
            success: function(data) {
                data = JSON.parse(data);
                data.unshift({id:'', text:''});

                select2KtpKecamatan.html('').select2({
                    allowClear: true,
                    placeholder: '--- Pilih ---',
                    data: data,
                    disabled: false
                });
            }
        });

    });

    select2DomisiliProvinsi.on('select2:select', function (e) {
        console.log(e);
        var data = e.params.data;
        var idProvinsi = data.id;

        select2DomisiliKota.html('').select2({
            allowClear: true,
            placeholder: 'Loading data...',
            data: [{id: '', text: ''}],
            disabled: true
        });

        $.ajax({
            type: 'POST',
            url: "{{ url('karyawan') }}" + '/kota',
            data: {
                '_token': csrf_token,
                'id': data.id,
            },
            success: function(data) {
                data = JSON.parse(data);
                data.unshift({id:'', text:''});

                select2DomisiliKota.html('').select2({
                    allowClear: true,
                    placeholder: '--- Pilih ---',
                    data: data,
                    disabled: false
                });
            }
        });

    });

    select2DomisiliKota.on('select2:select', function (e) {
        console.log(e);
        var data = e.params.data;
        var idKota = data.id;

        select2DomisiliKecamatan.html('').select2({
            allowClear: true,
            placeholder: 'Loading data...',
            data: [{id: '', text: ''}],
            disabled: true
        });

        $.ajax({
            type: 'POST',
            url: "{{ url('karyawan') }}" + '/kecamatan',
            data: {
                '_token': csrf_token,
                'id': data.id,
            },
            success: function(data) {
                data = JSON.parse(data);
                data.unshift({id:'', text:''});

                select2DomisiliKecamatan.html('').select2({
                    allowClear: true,
                    placeholder: '--- Pilih ---',
                    data: data,
                    disabled: false
                });
            }
        });

    });

    $('#alamat-domisili-checkbox').change(function(){
        if ($(this).is(':checked')) {
            $('#wrapper-domisili').hide();
            select2DomisiliProvinsi.val(select2KtpProvinsi.val());
            select2DomisiliKota.val(select2KtpKota.val());
            select2DomisiliKecamatan.val(select2KtpKecamatan.val());
            domisiliDetail.val(ktpDetail.val());
            select2DomisiliProvinsi.trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: select2KtpProvinsi.select2('data')[0]['id'],
                        text: select2KtpProvinsi.select2('data')[0]['text']
                    }
                }
            });
            select2DomisiliProvinsi.trigger('change');

            setTimeout(function(){
                select2DomisiliKota.trigger({
                    type: 'select2:select',
                    params: {
                        data: {
                            id: select2KtpKota.select2('data')[0]['id'],
                            text: select2KtpKota.select2('data')[0]['text']
                        }
                    }
                });
                select2DomisiliKota.val(select2KtpKota.val());
                select2DomisiliKota.trigger('change');
            }, 1000);

            setTimeout(function(){
                select2DomisiliKecamatan.val(select2KtpKecamatan.val());
                select2DomisiliKecamatan.trigger('change');
            }, 2000);
        } else {
            $('#wrapper-domisili').show();
        }
    });

    ktpDetail.on('keyup paste', function() {
        if ($('#alamat-domisili-checkbox').is(':checked')) {
            domisiliDetail.val(ktpDetail.val());
            validator.element(domisiliDetail);
        }
    })
});
</script>
@endpush
