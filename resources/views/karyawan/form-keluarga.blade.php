@extends('layouts.app')

@isset($keluarga)
    @section('title', 'Ubah Keluarga - '.$karyawan->nama)

    @php
        $action = route('karyawan.edit-keluarga', compact('karyawan', 'keluarga'));
        $method = method_field('PUT');
    @endphp
@endisset

@push('stylesheets')
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('breadcrumb')
    {{ Breadcrumbs::render('karyawan.add-keluarga', $karyawan) }}
@endsection

@empty($keluarga)
    @section('title', 'Tambah Keluarga - '.$karyawan->nama)

    @php
        $keluarga = new \App\Keluarga();
        $action = route('karyawan.add-keluarga', $karyawan);
        $method = method_field('POST');
    @endphp
@endempty

@section('content')
<section class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
    <h5 class="hk-sec-title">Tambah Keluarga {{ $karyawan->nama }}</h5>
    <form id="myform" action="{{ $action }}" method="POST">
        {{ $method }}
        @csrf
        <input name="id_karyawan" type="hidden" class="form-control" value="{{ $karyawan->id_karyawan }}">
        <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>
            <input name="nama_lengkap" type="text" class="form-control" value="{{ $keluarga->nama_lengkap }}">
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-xs-12">
                <label for="nik">Nomor KTP (NIK)</label>
                <input name="nik" type="text" class="form-control" value="{{ $keluarga->nik }}">
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label for="no_kk">Nomer KK</label>
                <input name="no_kk" type="text" class="form-control" value="{{ $keluarga->no_kk }}">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label mb-10">Jenis Kelamin</label>
            <div class="pl-3">
                <div class="custom-radio form-check-inline">
                    <input id="radio_male" value="Laki-laki" name="jenis_kelamin" class="custom-control-input form-check-input"
                    @if ($keluarga->jenis_kelamin != "Perempuan")
                        checked
                    @endif
                    type="radio">
                    <label for="radio_male" class="custom-control-label">Laki-laki</label>
                </div>
                <div class="custom-radio form-check-inline">
                    <input id="radio_female" value="Perempuan" name="jenis_kelamin" class="custom-control-input form-check-input"
                    @if ($keluarga->jenis_kelamin == "Perempuan")
                        checked
                    @endif
                    type="radio">
                    <label for="radio_female" class="custom-control-label">Perempuan</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-xs-12">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input name="tempat_lahir" type="text" placeholder="Nama Kabupaten/Kota" class="form-control" value="{{ $keluarga->tempat_lahir }}">
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input name="tgl_lahir" type="text" class="form-control single-date-picker" value="{{ $keluarga->tgl_lahir }}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6 col-xs-12">
                <label for="status_perkawinan">Status Kawin</label>
                <select name="status_perkawinan" class="form-control select2">
                    <option></option>
                    @foreach (['Belum kawin', 'Sudah kawin'] as $text)
                        <option value="{{ $text }}" @if ($keluarga->status_perkawinan == $text)
                            selected
                        @endif>{{ $text }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label for="status_keluarga">Hubungan dengan karyawan</label>
                <select name="status_keluarga" class="form-control select2">
                    <option></option>
                    @foreach (['Ayah', 'Ibu', 'Istri', 'Suami', 'Anak', 'Kakak', 'Adik', 'Kakek', 'Nenek', 'Paman', 'Bibi', 'Keponakan'] as $item)
                        <option value="{{ $item }}" @if ($keluarga->status_keluarga == $item)
                            selected
                        @endif>{{ $item }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">
            Simpan
		</button>
    </form>
</section>
@endsection

@push('scripts')

<!-- Jasny-bootstrap  JavaScript -->
<script src="{{ asset('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Form Wizard JavaScript -->
<script src="{{ asset('vendors/jquery-steps/build/jquery.steps.min.js') }}"></script>
<!-- Dropify JavaScript -->
<script src="{{ asset('vendors/dropify/dist/js/dropify.min.js') }}"></script>
<!-- Laravel Javascript Validation -->
<script type="text/javascript" src="{{ url('js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\GeneralRequest', '#myform') !!}

<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function(){
    var myform = $('#myform');
    var validator = $('#myform').data("validator");

    $('.timepicker').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        "cancelClass": "btn-secondary",
        locale: {
		  format: 'H:mm'
        },
        showDropdowns: true,
    });

    $('.single-date-picker').daterangepicker({
        singleDatePicker: true,
		showDropdowns: true,
        cancelClass: "btn-secondary",
        locale: {
           format: 'DD/MM/YYYY'
        }
    });

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    $(document).on("change", ".select2", function() {
        validator.element(this);
    });

	$('.timepicker').focusin(function(){
		$('.calendar-table').css("display","none");
    });
});
</script>
@endpush
