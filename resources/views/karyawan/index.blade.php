@extends('layouts.app')

@section('title', 'Data Karyawan')

@push('stylesheets')
<!-- Data Table CSS -->
<link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<div class="card">
    <div class="card-header card-header-action">
        <h5>Filter</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-6 col-xs-12">
                <label for="filter-seksi">Seksi</label>
                {{-- <input id="filter-seksi" type="text" class="form-control single-date-picker" value="{{ date('d/m/Y') }}"> --}}
                <select id="filter-seksi" class="form-control select2">
                    <option></option>
                    @foreach ($seksi as $item)
                        <option value="{{ $item->seksi }}">{{ $item->seksi }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label for="filter-jabatan">Jabatan</label>
                {{-- <input id="filter-jabatan" type="text" class="form-control single-date-picker" value="{{ date('d/m/Y') }}"> --}}
                <select id="filter-jabatan" class="form-control select2">
                    <option></option>
                    @foreach (\App\Jabatan::all() as $item)
                        <option value="{{ $item->id_jabatan }}">{{ " $item->nama" }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12 text-center">
                <button id="btn-filter" class="btn btn-sm btn-outline-success btn-rounded w-20"><i class="fa fa-filter"></i> Filter</button>
                <button id="btn-generate" class="btn btn-sm btn-outline-info btn-rounded w-20"><i class="fa fa-file"></i> Generate Excel</button>
                <a id="btn-download" style="display: none" href="{{ asset('excel/data_karyawan_SPN.xlsx') }}" class="btn btn-sm btn-outline-danger btn-rounded w-20" download>
                    <i class="fa fa-download"></i> Download Excel
                </a>
            </div>
        </div>
    </div>
</div>

<section class="card">
    <div class="card-header card-header-action">
        <h5>Data Karyawan</h5>
        <div class="d-flex align-items-center">
            <button id="btn-sync" class="btn btn-sm btn-primary">
                <i class="fa fa-refresh"></i> Sinkronisasi Karyawan
            </button>
        </div>
    </div>
    <div class="card-body table-responsive w-100">
        <table id="mydatatable" class="table w-100 pb-30">
            <thead>
                <tr>
                    <th class="no-filter">#</th>
                    <th>PRN</th>
                    <th>Nama</th>
                    <th>Jabatan</th>
                    <th>Seksi</th>
                    <th>Gaji</th>
                    <th>Status</th>
                    <th class="text-center no-filter" style="width: 167px;"></th>
                </tr>
            </thead>
        </table>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Edit Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="edit-form" onsubmit="event.preventDefault();">
                    @csrf
                    {{ method_field('PUT') }}
                    @csrf
                    <input type="hidden" name="id_karyawan">
                    <div class="row">
                        <div class="form-group col-12">
                            <label>Gaji Pokok</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input class="form-control" name="gaji_pokok" type="number">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-update" type="button" class="btn btn-success" data-dismiss="modal">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var ajaxData = function(d){
        d._token = csrf_token;
        d.seksi = $('#filter-seksi').val();
        d.id_jabatan = $('#filter-jabatan').val();
    };

    var mytable = myDataTable('#mydatatable', '{!! route('karyawan.data') !!}', [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'prn', name: 'prn' },
            { data: 'nama', name: 'nama' },
            { data: 'jabatan', name: 'jabatan' },
            { data: 'seksi', name: 'seksi' },
            { data: 'gaji_pokok', name: 'gaji_pokok' },
            { data: 'status', name: 'status' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ], [1, "asc"], ajaxData);

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    $('#btn-filter').on('click', function(e) {
        // if ($('#filter-seksi').val() != "" ) {
            Swal.fire({
                title: "Please wait",
                text: "Reload data karyawan",
                timer: 2000
            });
            Swal.showLoading();
            mytable.ajax.reload();
        // }
    });

    $('#btn-generate').on('click', function(e) {
        generateExcel();
    });

    $(document).on('click', '#btn-sync', function(e) {
        swalSync();
    });

    $(document).on("click", "#btn-edit", function () {
    	showEditModal($(this).attr("value"));
    });

    $("#btn-update").on("click", function () {
        saveEditModal();
    });

    function generateExcel() {
        $.ajax({
            url: "{{ route('karyawan.excel') }}",
            type: "POST",
            data: {
                '_method' : 'POST',
                '_token' : csrf_token
            },
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang menggenerate excel',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
                $('#btn-download').hide();
            },
            success: function(data) {
                mytable.ajax.reload();
                Swal.close();
                $('#btn-download').show();
                swal({
                    title: "Berhasil menggenereate excel!",
                    text: "Silahkan download file excel",
                    icon: "success",
                    timer: 1500
                });
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    }

    function swalSync() {
        swal({
            title: "Apakah Anda yakin?",
            text: "Proses Sinkronisasi akan mensinkronkan data karyawan yang ada pada sistem HRD",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willSync) => {
            if (willSync) {
                $.ajax({
                    url: "{{ route('karyawan.sync') }}", //ganti dewe
                    type: "GET",
                    data: {
                        '_method' : 'GET',
                        '_token' : csrf_token
                    },
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Please wait',
                            text: 'Sedang melakukan sinkronisasi data',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                        Swal.showLoading();
                    },
                    success: function(data) {
                        mytable.ajax.reload();
                        Swal.close();
                        swal({
                            title: "Berhasil Mensinkronkan data!",
                            text: "Data karyawan berhasil disinkronisasi",
                            icon: "success",
                            timer: 2000
                        });
                    },
                    error : function(data) {
                        Swal.close();
                        swal({
                            title: "Oops...",
                            text: data.responseJSON.message,
                            icon: "error",
                            timer: 2000
                        });
                    }
                })
            }
        });
    }

    function showEditModal(id) {
        $.ajax({
            url: "{{ url('karyawan') }}" + "/" + id,
            type: "GET",
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang memuat data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                data = JSON.parse(data);
                if (data) {
                	$("#edit-form [name='gaji_pokok']").val(data["gaji_pokok"]);
                	$("#edit-form [name='status']").val(data["status"]);
                	$("#edit-form [name='bpjs_kesehatan_in']").val(data["bpjs_kesehatan_in"]);
                	$("#edit-form [name='bpjs_kesehatan_p_out']").val(data["bpjs_kesehatan_p_out"]);
                	$("#edit-form [name='bpjs_kesehatan_out']").val(data["bpjs_kesehatan_out"]);
                    $("#edit-form [name='bpjs_jkk_in']").val(data["bpjs_jkk_in"]);
                    $("#edit-form [name='bpjs_jkk_p_out']").val(data["bpjs_jkk_p_out"]);
                    $("#edit-form [name='bpjs_jkm_in']").val(data["bpjs_jkm_in"]);
                    $("#edit-form [name='bpjs_jkm_p_out']").val(data["bpjs_jkm_p_out"]);
                    $("#edit-form [name='bpjs_jht_in']").val(data["bpjs_jht_in"]);
                    $("#edit-form [name='bpjs_jht_p_out']").val(data["bpjs_jht_p_out"]);
                    $("#edit-form [name='bpjs_jht_out']").val(data["bpjs_jht_out"]);
                    $("#edit-form [name='bpjs_jp_in']").val(data["bpjs_jp_in"]);
                    $("#edit-form [name='bpjs_jp_p_out']").val(data["bpjs_jp_p_out"]);
                    $("#edit-form [name='bpjs_jp_out']").val(data["bpjs_jp_out"]);
                	$("#edit-form [name='id_karyawan']").val(id);
                    $("#myModal").modal("show");
                }
                Swal.close();
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };

    function saveEditModal() {
    	var myForm = $('#edit-form')[0];
        var formData = new FormData(myForm);

        $.ajax({
            url: "{{ url('karyawan') }}" + "/"+ $("#edit-form [name='id_karyawan']").val(),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang menyimpan data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                mytable.ajax.reload( null, false );
                Swal.close();
                swal({
                    title: "Berhasil mengupdate data!",
                    text: "Data karyawan berhasil diupdate",
                    icon: "success",
                    timer: 2000
                });
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };
});
</script>
@endpush
