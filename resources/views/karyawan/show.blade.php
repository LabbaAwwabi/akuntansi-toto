@extends('layouts.app')

@section('breadcrumb')
    {{ Breadcrumbs::render('karyawan.show', $karyawan) }}
@endsection

@push('stylesheets')
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    .font-black {
        color: #212529;
    }
</style>
@endpush

@section('content')
@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="profile-cover-wrap overlay-wrap">
    <div class="profile-cover-img" style="background-image:url('{{ asset('dist/img/trans-bg.jpg') }}');"></div>
    <div class="bg-overlay bg-trans-dark-60"></div>
    <div class="container profile-cover-content py-50">
        <div class="hk-row">
            <div class="col-lg-8">
                <div class="media align-items-center">
                    <div class="media-img-wrap  d-flex">
                        <div class="avatar">
                            @php
                            if ($karyawan->foto != null) {
                                $foto = asset('foto/'.$karyawan->foto);
                            }else {
                                $foto = asset('dist/img/avatar12.jpg');
                            }
                            @endphp
                            <img src="{{ $foto }}" alt="user" class="avatar-img rounded-circle">
                        </div>
                    </div>
                    <div class="media-body">
                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{ $karyawan->nama }}</div>
                        <div class="font-14 text-white"><span class="mr-5">Jabatan {{ $karyawan->jabatan->nama }}</span> - <span>Seksi {{ $karyawan->seksi->nama }}</span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="button-list">
                    <a href="{{ route('karyawan.edit', $karyawan) }}" class="btn btn-dark btn-wth-icon icon-wthot-bg btn-rounded"><span class="btn-text">Edit</span><span class="icon-label"><i class="icon ion-md-create"></i> </span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-white shadow-bottom">
    <div class="container">
        <ul class="nav nav-light nav-tabs" role="tablist">
            <li class="nav-item">
                <a href="#biodata" role="tab" data-toggle="tab" class="d-flex h-60p align-items-center nav-link active">Biodata</a>
            </li>
            <li class="nav-item">
                <a href="#kepegawaian" role="tab" data-toggle="tab" class="d-flex h-60p align-items-center nav-link">Kepegawaian</a>
            </li>
            <li class="nav-item">
                <a href="#keluarga" role="tab" data-toggle="tab" class="d-flex h-60p align-items-center nav-link">Keluarga&nbsp;<span id="badge-keluarga" class="badge badge-primary">{{ $karyawan->keluarga->count() }}</span></a>
            </li>
        </ul>
    </div>
</div>
<div class="tab-content mt-sm-20 mt-10">
    <div id="biodata" class="tab-pane fade show active" role="tabpanel">
        <div class="card">
            @if ($karyawan->foto != null)
                <div class="position-relative mt-10">
                    <img class="card-img-top img-responsive img-thumbnail rounded mx-auto d-block" src="{{ asset('foto/'.$karyawan->foto) }}" alt="Foto Karyawan"
                    style="width: auto; max-height: 300px;">
                </div>
            @endif
            <div class="card-body w-100 p-0">
                <table class="table mb-0" style="width: 100%">
                    <thead>
                        <tr>
                            <td class="p-0" style="width:20%; border-top: none;"></td>
                            <td class="p-0" style="width:28%; border-top: none;"></td>
                            <td class="p-0" style="width:4%; border: none;"></td>
                            <td class="p-0" style="width:20%; border-top: none;"></td>
                            <td class="p-0" style="width:28%; border-top: none;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="border-top: none;">Nama Lengkap</td>
                            <td style="border-top: none;" class="font-black">{{ $karyawan->nama }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td style="border-top: none;">Nomer KTP (NIK)</td>
                            <td style="border-top: none;" class="font-black">{{ $karyawan->no_ktp }}</td>
                        </tr>
                        <tr>
                            <td>Jenis kelamin</td>
                            <td class="font-black">{{ $karyawan->jenis_kelamin }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td>Agama</td>
                            <td class="font-black">{{ $karyawan->agama }}</td>
                        </tr>
                        <tr>
                            <td>Tempat, Tanggal lahir</td>
                            <td class="font-black">{{ $karyawan->tempat_lahir }}, <span format-tgl="DD MMMM YYYY" format-tgl-value="{{ $karyawan->tgl_lahir }}" class="font-black">{{ $karyawan->tgl_lahir }}</span></td>
                            <td style="border: none;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td>Alamat KTP</td>
                            <td></td>
                            <td style="width:4%; border: none;"></td>
                            <td>Alamat Domisili</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border-top: none;">&emsp;Provinsi</td>
                            <td style="border-top: none;" class="font-black">{{ $karyawan->nama_provinsi_ktp }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td style="border-top: none;" style="border-top: none;">&emsp;Provinsi</td>
                            <td class="font-black">{{ $karyawan->nama_provinsi_domisili }}</td>
                        </tr>
                            <td style="border-top: none;">&emsp;Kabupaten</td>
                            <td class="font-black">{{ $karyawan->nama_kota_ktp }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td style="border-top: none;">&emsp;Kabupaten</td>
                            <td class="font-black">{{ $karyawan->nama_kota_domisili }}</td>
                        </tr>
                        <tr>
                            <td style="border-top: none;">&emsp;Kecamatan</td>
                            <td class="font-black">{{ $karyawan->nama_kecamatan_ktp }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td style="border-top: none;">&emsp;Kecamatan</td>
                            <td class="font-black">{{ $karyawan->nama_kecamatan_domisili }}</td>
                        </tr>
                        <tr>
                            <td style="border-top: none;">&emsp;Detail</td>
                            <td class="font-black">{{ $karyawan->alamat_ktp }}</td>
                            <td style="width:4%; border: none;"></td>
                            <td style="border-top: none;">&emsp;Detail</td>
                            <td class="font-black">{{ $karyawan->alamat_ktp }}</td>
                        </tr>
                        <tr>
                            <td>Pendidikan Terakhir</td>
                            <td class="font-black">{{ $karyawan->pendidikan_terakhir }}</td>
                            <td style="border: none;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td><a href = "mailto: {{ $karyawan->email }}" target="_blank" class="font-black">{{ $karyawan->email }}</a></td>
                            <td style="width:4%; border: none;"></td>
                            <td>Nomer HP</td>
                            <td class="font-black">{{ $karyawan->no_hp }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="kepegawaian" class="tab-pane fade" role="tabpanel">
        <div class="card">
            <div class="card-body w-100 p-0">
                <table class="table mb-0" style="width: 100%">
                    <thead>
                        <tr>
                            <td class="p-0" style="width:20%; border-top: none;"></td>
                            <td class="p-0" style="width:28%; border-top: none;"></td>
                            <td class="p-0" style="width:4%; border: none;"></td>
                            <td class="p-0" style="width:20%; border-top: none;"></td>
                            <td class="p-0" style="width:28%; border-top: none;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="border-top: none;">PRN</td>
                            <td style="border-top: none;" class="font-black">{{ $karyawan->prn }}</td>
                            <td style="border: none;"></td>
                            <td style="border-top: none;">Status Karyawan</td>
                            <td style="border-top: none;"class="font-black">{{ $karyawan->statusKaryawan->nama }}</td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td class="font-black">{{ $karyawan->jabatan->nama }}</td>
                            <td style="border: none;"></td>
                            <td>Seksi</td>
                            <td class="font-black">{{ $karyawan->seksi->nama }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Masuk</td>
                            <td class="font-black"><span format-tgl="DD MMMM YYYY" format-tgl-value="{{ $karyawan->tgl_masuk }}"></span></td>
                            <td style="border: none;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td>No. Rekening</td>
                            <td class="font-black">{{ ($karyawan->no_rekening)? $karyawan->no_rekening.' ('.$karyawan->nama_bank.')': '-'}}</td>
                            <td style="border: none;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td>No. NPWP</td>
                            <td class="font-black">{{ $karyawan->no_npwp }}</td>
                            <td style="border: none;" colspan="3"></td>
                        </tr>
                        <tr>
                            <td>No. BPJS Kesehatan</td>
                            <td class="font-black">{{ $karyawan->bpjs_kesehatan }}</td>
                            <td style="border: none;"></td>
                            <td>No. BPJS Ketenagakerjaan</td>
                            <td class="font-black">{{ $karyawan->bpjs_ketenagakerjaan }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="keluarga" class="tab-pane fade container-fluid" role="tabpanel">
        <div class="row">
            <div class="col-md-8 offset-md-2 col-xs-12 px-0">
                <div class="card">
                    <div class="card-header card-header-action">
                        <h5>Info Emergency Call</h5>
                        <div class="d-flex align-items-center">
                            <a href="{{ route('karyawan.add-keluarga', ['id_karyawan' => $karyawan->id_karyawan]) }}" class="btn btn-sm btn-success">
                                <i class="fa fa-plus"></i> Tambah Keluarga
                            </a>
                        </div>
                    </div>
                    <div class="card-body table-responsive w-100">
                        <div class="table-responsive">
                            <table id="myKeluargaTable" class="table w-100">
                                <thead>
                                    <tr>
                                        <td class="p-0" style="width:40%; border-top: none;"></td>
                                        <td class="p-0" style="width:60%; border-top: none;"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="border-top: none;">Nomor Telpon/HP</td>
                                        <td class="font-black" style="border-top: none;">{{ $karyawan->no_emergency_call }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td class="font-black">{{ $karyawan->nama_emergency_call }}</td>
                                    </tr>
                                    <tr>
                                        <td>Relasi dengan Karyawan</td>
                                        <td class="font-black">{{ $karyawan->relasi_emergency_call }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @php
                $rowFlag = 1;
            @endphp
            @foreach ($karyawan->keluarga as $keluarga)
                @php
                    if ($rowFlag % 2 == 1) {
                        $flag = "pl-0";
                    } else {
                        $flag = "pr-0";
                    }
                    $rowFlag++;
                @endphp
                <div id="col-keluarga-{{ ($rowFlag - 1) }}" class="col-md-6 col-xs-12 {{ $flag }} col-keluarga">
                    <div class="card">
                        <div class="card-header card-header-action">
                            <h5>Keluarga {{ ($rowFlag - 1) }}</h5>
                            <div class="d-flex align-items-center">
                                <div class="button-group">
                                    <a href="{{ route('karyawan.edit-keluarga', ['karyawan' => $karyawan,'keluarga'=> $keluarga]) }}"
                                        class="btn btn-primary btn-sm btn-icon btn-icon-style-1">
                                        <span class="btn-icon-wrap"><i class="material-icons">edit</i></span>
                                    </a>
                                    <button id="btn-delete" value="{{ $keluarga->id_keluarga }}" class="btn btn-danger btn-sm btn-icon btn-icon-style-1">
                                        <span class="btn-icon-wrap"><i class="material-icons">delete_forever</i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive w-100 p-0">
                            <div class="table-responsive">
                                <table id="myKeluargaTable" class="table w-100 mb-0">
                                    <thead>
                                        <tr>
                                            <td class="p-0" style="width:40%; border-top: none;"></td>
                                            <td class="p-0" style="width:60%; border-top: none;"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="border-top: none;">Nama Lengkap</td>
                                            <td class="font-black" style="border-top: none;">{{ $keluarga->nama_lengkap }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Keluarga</td>
                                            <td class="font-black">{{ $keluarga->status_keluarga }}</td>
                                        </tr>
                                        <tr>
                                            <td>Usia</td>
                                            <td class="font-black">{{ $keluarga->usia }}</td>
                                        </tr>
                                        <tr>
                                            <td>No KTP (NIK)</td>
                                            <td class="font-black">{{ $keluarga->nik }}</td>
                                        </tr>
                                        <tr>
                                            <td>No KK</td>
                                            <td class="font-black">{{ $keluarga->no_kk }}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Kelamin</td>
                                            <td class="font-black">{{ $keluarga->jenis_kelamin }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tempat Lahir</td>
                                            <td class="font-black">{{ $keluarga->tempat_lahir }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Lahir</td>
                                            <td class="font-black">{{ $keluarga->tgl_lahir }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Perkawinan</td>
                                            <td class="font-black">{{ $keluarga->status_perkawinan }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<!-- Data Table JavaScript -->
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    $(document).on('click', '#btn-delete', function(e) {
        e.preventDefault();

        var id_keluarga = $(this).attr('value');
        swalDelete(id_keluarga, $(this).closest('.col-keluarga'));
    });

    function swalDelete(id_keluarga, element) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: "Apakah Anda yakin?",
            text: "Setelah dihapus, Anda tidak akan dapat memulihkan data ini!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "{{ url('karyawan') }}" + '/{{ $karyawan->id_karyawan }}/keluarga/' +  id_keluarga,
                    type: "POST",
                    data: {
                        '_method' : 'DELETE',
                        '_token' : csrf_token
                    },
                    success: function(data) {
                        swal({
                            title: "Berhasil Menghapus!",
                            text: "Data karyawan berhasil dihapus",
                            icon: "success",
                            timer: 2000
                        });
                        normalizeHtml(element);
                    },
                    error : function(data) {
                        swal({
                            title: "Oops...",
                            text: data.responseJSON.message,
                            icon: "error",
                            timer: 2000
                        });
                    }
                })
            }
        });
    }

    function normalizeHtml(thisElement) {
        var count = Number($('#badge-keluarga').html());
        $('#badge-keluarga').html(--count);

        if (thisElement.next().length) {
            nextElement = thisElement.next();
            thisElement.remove();

            do {
                thisElement = nextElement;

                if (thisElement.hasClass('pl-0')) {
                    thisElement.removeClass('pl-0');
                    thisElement.addClass('pr-0');
                } else {
                    thisElement.removeClass('pr-0');
                    thisElement.addClass('pl-0');
                }

                nextElement = thisElement.next();
            } while (nextElement.length);
        } else {
            thisElement.remove();
        }
    }
});
</script>
@endpush
