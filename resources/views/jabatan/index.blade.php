@extends('layouts.app')

@section('title', 'Data Jabatan')

@push('stylesheets')
<!-- Data Table CSS -->
<link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- select2 CSS -->
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<section class="card">
    <div class="card-header card-header-action">
        <h5>Data Jabatan</h5>
    </div>
    <div class="card-body table-responsive w-100">
        <table id="mydatatable" class="table w-100 pb-30">
            <thead>
                <tr>
                    <th class="no-filter">#</th>
                    <th>Nama</th>
                    <th>Tunjangan Jabatan</th>
                    <th>Tunjangan Shift</th>
                    <th>Lembur 4 jam</th>
                    <th>Lembur 8 jam</th>
                    <th class="text-center no-filter" style="width: 167px;"></th>
                </tr>
            </thead>
        </table>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Lain-Lain</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="edit-form" onsubmit="event.preventDefault();">
                    @csrf
                    {{ method_field('PUT') }}
                    @csrf
                    <input type="hidden" name="id_jabatan">
                    <div class="form-group">
                        <label for="tunjangan_jabatan">Tunjangan Jabatan</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="tunjangan_jabatan" type="number" min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lembur_4jam">Lembur 4 jam</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="lembur_4jam" type="number" min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lembur_8jam">Lembur 8 jam</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="lembur_8jam" type="number" min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tunjangan_shift">Tunjangan Shift</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control" name="tunjangan_shift" type="number" min="0">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-update" type="button" class="btn btn-success" data-dismiss="modal">
                    <i class="fa fa-save"></i> Simpan
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<!-- Select2 JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<!-- Swal -->
<script src="{{ asset('js/sweetalert2@8.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

    var mytable = myDataTable('#mydatatable', '{!! route('jabatan.data') !!}', [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'nama', name: 'nama' },
            { data: 'tunjangan_jabatan', name: 'tunjangan_jabatan' },
            { data: 'tunjangan_shift', name: 'tunjangan_shift' },
            { data: 'lembur_4jam', name: 'lembur_4jam' },
            { data: 'lembur_8jam', name: 'lembur_8jam' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ], [2, "desc"]);


    $(document).on("click", "#btn-edit", function () {
    	showEditModal($(this).attr("value"));
    });

    $("#btn-update").on("click", function () {
        saveEditModal();
    });

    function showEditModal(id) {
        $.ajax({
            url: "{{ url('jabatan') }}" + "/" + id,
            type: "GET",
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang memuat data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                data = JSON.parse(data);
                if (data) {
                	$("#edit-form [name='tunjangan_jabatan']").val(data["tunjangan_jabatan"]);
                	$("#edit-form [name='tunjangan_shift']").val(data["tunjangan_shift"]);
                	$("#edit-form [name='lembur_4jam']").val(data["lembur_4jam"]);
                	$("#edit-form [name='lembur_8jam']").val(data["lembur_8jam"]);
                	$("#edit-form [name='id_jabatan']").val(id);
                    $("#myModal").modal("show");
                }
                Swal.close();
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };

    function saveEditModal() {
    	var myForm = $('#edit-form')[0];
        var formData = new FormData(myForm);

        $.ajax({
            url: "{{ url('jabatan') }}" + "/"+ $("#edit-form [name='id_jabatan']").val(),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                Swal.fire({
                    title: 'Please wait',
                    text: 'Sedang menyimpan data',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success: function(data) {
                mytable.ajax.reload( null, false );
                Swal.close();
                swal({
                    title: "Berhasil mengupdate data!",
                    text: "Data jabatan berhasil diupdate",
                    icon: "success",
                    timer: 2000
                });
            },
            error : function(data) {
                Swal.close();
                swal({
                    title: "Oops...",
                    text: data.responseJSON.message,
                    icon: "error",
                    timer: 2000
                });
            }
        })
    };
});
</script>
@endpush
