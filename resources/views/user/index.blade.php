@extends('layouts.app')

@section('title', 'Data User')

@push('stylesheets')
    <!-- Data Table CSS -->
    <link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div class="row">
    <div class="col-lg-12 col-sm-12">
        <section class="card">
            <div class="card-header card-header-action">
                <h5>Data User</h5>
                <div class="d-flex align-items-center">
                    <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">
                        <i class="ion ion-md-person-add"></i> Tambah User
                    </a>
                </div>
            </div>
            <div class="card-body table-responsive w-100">
                <table id="user-table" class="table w-100 pb-30">
                    <thead>
                        <tr>
                            <th class="no-filter">#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th class="text-center no-filter" style="width: 167px;"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        @include('layouts.left')
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    var userTable = myDataTable('#user-table', '{!! route('user.data') !!}', [
        { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
        { data: 'nama', name: 'nama' },
        { data: 'email', name: 'email' },
        { data: 'action', name: 'action', orderable: false, searchable: false }
    ]);

    $(document).on('click', '#user-btn-delete', function(e) {
        e.preventDefault();

        var id = $(this).attr('value');
        swalDelete(id, 'user');
    });

    function swalDelete(id, model) {
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: "Apakah Anda yakin?",
            text: "Setelah dihapus, Anda tidak akan dapat memulihkan data ini!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showLoaderOnConfirm: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "{{ url('/') }}" + '/' + model + '/' +  id,
                    type: "POST",
                    data: {
                        '_method' : 'DELETE',
                        '_token' : csrf_token
                    },
                    success: function(data) {
                        userTable.ajax.reload();

                        swal({
                            title: "Berhasil Menghapus!",
                            text: "Data " + model + " berhasil dihapus",
                            icon: "success",
                            timer: 2000
                        });
                    },
                    error : function(data) {
                        swal({
                            title: "Oops...",
                            text: data.responseJSON.message,
                            icon: "error",
                            timer: 2000
                        });
                    }
                })
            }
        });
    }
});
</script>
@endpush
