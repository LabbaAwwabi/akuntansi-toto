@extends('layouts.app')

@section('title', 'Profil')

@php
    $action = route('user.update', $user);
    $method = method_field('PUT');
@endphp

@push('stylesheets')
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<section class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
    <h5 class="hk-sec-title">Profil</h5>
    @if (session('change_pass_notif'))
        <div class="alert alert-{{ session('change_pass_notif')[0] }}">{{ session('change_pass_notif')[1] }}</div>
    @endif
    <form id="myform" action="{{ $action }}" method="POST">
        {{ $method }}
        @csrf
        <div class="form-group">
            <label for="email">Email</label>
            <input name="email" type="email" class="form-control" value="{{ $user->email }}">
        </div>

        <div class="form-group">
            <label for="name">Nama</label>
            <input name="name" type="text" class="form-control" value="{{ $user->name }}">
        </div>

        <button type="submit" class="btn btn-primary">
            Simpan
		</button>
    </form>
</section>
@endsection

@push('scripts')
<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function(){
    $('.timepicker').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        "cancelClass": "btn-secondary",
        locale: {
		  format: 'H:mm'
        },
        showDropdowns: true,
    });

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    $('#dropdown-karwayan').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

	$('.timepicker').focusin(function(){
		$('.calendar-table').css("display","none");
    });
});
</script>
@endpush
