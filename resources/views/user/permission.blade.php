@extends('layouts.app')

@section('title', 'Data Permission')

@push('stylesheets')

    <!-- Data Table CSS -->
    <link href="{{ asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('breadcrumb')
    {{ Breadcrumbs::render('user.permission', $id_user) }}
@endsection
@php
    $action = route('user.create-permission');    
@endphp
@section('content')
<form id="myform" action="{{ $action }}" method="POST">
<input type="hidden" name="id_user" value="{{ $id_user }}">
@csrf
<div class="row">
    <div class="col-lg-12 col-sm-12">
        <section class="card">
            <div class="card-header card-header-action">
                <h5>Data Permission</h5>
                <div class="d-flex align-items-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="ion ion-md-person-add"></i> Simpan Permission
                    </button>
                </div>
            </div>
            <div class="card-body table-responsive w-100">
                <table id="permission-table" class="table w-100 pb-30">
                    <thead>
                        <tr>
                            <th class="no-filter">Menu</th>
                            <th>Permission</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1;
                            $guard_name = '';
                        @endphp
                        @foreach (\App\Permission::orderBy('guard_name', 'asc')->get() as $permission)
                        <tr>
                            <td>
                                @php
                                if($permission->guard_name !== $guard_name){
                                    $guard_name = $permission->guard_name;
                                    echo $permission->guard_name ;
                                }else{
                                    echo "";
                                }
                                @endphp
                                
                            </td>
                            <td>
                                <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" id="cb-permission-{{ $permission->id }}" type="checkbox" name="permission[]" value="{{ $permission->id }}" 
                                @if (in_array( $permission->id , $id_permission))
                                    checked
                                @endif>
                                <label class="custom-control-label" for="cb-permission-{{ $permission->id }}">{{ $permission->name }} {{ $permission->guard_name }}</label>
                                </div>
                            </td>
                        </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
</form>
@endsection

@push('scripts')
<!-- Data Table JavaScript -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/mydatatables.js') }}"></script>
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
$(document).ready(function() {
    //  $('#permission-table').DataTable({
    //      "scrollY":        "200px",
    //      "pageLength": 1000,
    //  });
});
</script>
@endpush
