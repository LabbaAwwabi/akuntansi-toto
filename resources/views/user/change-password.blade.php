@extends('layouts.app')

@push('stylesheets')
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('title', 'Ubah Password')

@php
    $action = route('profile.change-password');
    $method = method_field('PUT');
@endphp

@section('content')
<section class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
    <h5 class="hk-sec-title">Ubah Password</h5>
    @if (session('change_pass_notif'))
        <div class="alert alert-{{ session('change_pass_notif')[0] }}">{{ session('change_pass_notif')[1] }}</div>
    @endif
    <form id="myform" action="{{ $action }}" method="POST">
        {{ $method }}
        @csrf
        <div class="form-group">
            <label for="nama">Password lama</label>
            <input name="old_password" type="password" class="form-control">
        </div>
        <div class="form-group">
            <label for="nama">Password baru</label>
            <input name="new_password" type="password" class="form-control">
        </div>
        <div class="form-group">
            <label for="nama">Ulangi password</label>
            <input name="retype_password" type="password" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">
            Simpan
		</button>
    </form>
</section>
@endsection

@push('scripts')
<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function(){

});
</script>
@endpush
