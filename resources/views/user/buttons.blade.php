<div class="button-group">
    {{-- <a href="{{ route('shift.show', $shift) }}" class="btn btn-info btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">pageview</i></span></a> --}}
    <a href="{{ route('user.edit', $user) }}" class="btn btn-success btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">edit</i></span></a>
    <button id="user-btn-delete" value="{{ $user->id_user }}" class="btn btn-danger btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">delete_forever</i></span></button>
    {{-- <a href="{{ route('user.permission', $user) }}" class="btn btn-primary btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">edit</i></span></a>
    <a href="{{ route('user.role', $user) }}" class="btn btn-warning btn-sm btn-icon btn-icon-style-1"><span class="btn-icon-wrap"><i class="material-icons">edit</i></span></a> --}}
</div>
