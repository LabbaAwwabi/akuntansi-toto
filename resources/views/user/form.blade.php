@extends('layouts.app')

@push('stylesheets')
<!-- Daterangepicker CSS -->
<link href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@isset($user)
    @section('title', 'Ubah User')

    @php
        $action = route('user.update', $user);
        $method = method_field('PUT');
    @endphp
@endisset

@empty($user)
    @section('title', 'Tambah User')

    @php
        $user = new \App\User();
        $action = route('user.store');
        $method = method_field('POST');
    @endphp
@endempty

@section('content')
<section class="hk-sec-wrapper col-md-10 offset-md-1 col-sm-12">
    <h5 class="hk-sec-title">Form User</h5>
    <form id="myform" action="{{ $action }}" method="POST">
        {{ $method }}
        @csrf
        <div class="form-group">
            <label for="name">Nama</label>
            <input name="name" type="name" class="form-control" value="{{ $user->name }}">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input name="email" type="email" class="form-control" value="{{ $user->email }}">
        </div>
        <div class="form-group">
            <label for="nama">Password</label>
            <input name="password" type="password" class="form-control">
        </div>
        <div class="form-group">
            <label for="id_karyawan">Karyawan</label>
            <select name="id_karyawan" class="form-control select2" data-placeholder="Pilih" id="dropdown-karwayan" value = "2019">
                <option></option>
                @foreach (\App\Karyawan::orderBy('id_karyawan', 'asc')->get() as $karyawan)
                    <option value="{{ $karyawan->id_karyawan }}" @if ($karyawan->id_karyawan == $user->id_karyawan)
                            selected
                        @endif>{{ $karyawan->prn }} {{ $karyawan->nama }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="id_role">Role</label>
            <select name="id_roles[]" class="form-control select2" data-placeholder="Pilih" id="dropdown-role" value = "2019" multiple="multiple">
                <option></option>
                @foreach ($roles as $role)
                    <option value="{{ $role->id }}" @if ($user->hasRole($role))
                            selected
                        @endif>{{ $role->name }}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-primary">
            Simpan
		</button>
    </form>
</section>
@endsection

@push('scripts')
<!-- Daterangepicker JavaScript -->
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
<script>
$(document).ready(function(){
    $('.timepicker').daterangepicker({
        opens: 'left',
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        "cancelClass": "btn-secondary",
        locale: {
		  format: 'H:mm'
        },
        showDropdowns: true,
    });

    $('.select2').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

    $('#dropdown-karwayan').select2({
        allowClear: true,
        placeholder: '--- Pilih ---',
    });

	$('.timepicker').focusin(function(){
		$('.calendar-table').css("display","none");
    });
});
</script>
@endpush
