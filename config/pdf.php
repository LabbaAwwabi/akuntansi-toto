<?php

return [
	'mode'                 => '',
	'format'               => 'A5',
	'default_font_size'    => '8',
	'default_font'         => 'arial',
	'margin_left'          => 10,
	'margin_right'         => 10,
	'margin_top'           => 20,
	'margin_bottom'        => 20,
	'margin_header'        => 7,
	'margin_footer'        => 7,
	'orientation'          => 'P',
	'title'                => 'PT. SURYA PRATIWI NUSANTARA',
	'author'               => '',
	'watermark'            => '',
	'show_watermark'       => false,
	'watermark_font'       => 'arial',
	'display_mode'         => 'fullpage',
	'watermark_text_alpha' => 0.1,
	'custom_font_dir'      => '',
	'custom_font_data' 	   => [],
	'auto_language_detection'  => false,
	'temp_dir'               => '',
];
