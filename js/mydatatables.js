function myDataTable(selector, url, columns, defaultOrder, ajaxData){
  if(typeof defaultOrder == 'undefined'){
    defaultOrder = [ 1, "asc" ];
  }

  if (typeof ajaxData == 'undefined') {
      ajax = url;
  } else {
      ajax = {
        'url' : url,
        // 'type': 'POST',
        'data' : ajaxData
      };
  }

  var mytable = $(selector).DataTable( {
      processing : true,
      serverSide : true,
      ajax : ajax,
      columns : columns,
      columnDefs : [
          { 'targets': [0, 'no-filter'], 'orderable': false, 'searching': false },
          { 'targets': [ 'text-center' ], 'className': "text-center" }
        ],
      stateSave: true,
      order: [defaultOrder],
      language: {
          search: "",
          searchPlaceholder: "Search",
          processing: "<i class='fa fa-refresh fa-spin'></i> Loading...",
      },
      searchDelay: 700
  });

  $.fn.dataTable.ext.errMode = 'throw';

  return mytable;
}
